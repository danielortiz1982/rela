<div class="slideContainer">
	<div class="slideOverlay">
		<div class="container">
			
			<div class="slideControls">
				<div data-control="prev" class="slideControl prev"><i class="fa fa-angle-left"></i></div>
				<div data-control="next" class="slideControl next"><i class="fa fa-angle-right"></i></div>
			</div>

			<div class="slideContent">
				<h2 class="the_title"></h2>
				<h3 class="the_content"></h3>
				<div><a href="" class="home_slide_link_text" data-pageid=""></a></div>
			</div>

			<div class="slideButtons">
				<div class="slideButton">
					<i class="fa fa-circle"></i>
				</div>
			</div>

		</div>
	</div>
</div>