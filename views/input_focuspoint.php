<input type="text" class="focuspoint_meta" id="<?=$inputID?>" name="<?=$inputID?>" value='<?=$inputValue?>'>
<strong>Image Location</strong>
<input type="text" placeholder="exp. http://exmaple.com/image.png" class="focuspointURL<?=$inputID?>" name="focuspointURL<?=$inputID?>"/>
<button class="setFocuspoint<?=$inputID?> button button-large">Set</button>

<div class="focuspointContainer" data-metaID="<?=$inputID?>">
	<div id="Frames">
		<div class="focuspoint">
			<img/>
		</div>
	</div>

	<div id="Info">
		<div class="helper-tool">
			<h1>Click the image to set the FocusPoint.</h1>
			<div class="helper-tool-target">
				<img class="helper-tool-img">
				<img class="reticle" src="<?=$_SERVER['REQUEST_SCHEME']?>://<?=$_SERVER['HTTP_HOST']?><?=str_replace($_SERVER['DOCUMENT_ROOT'], '', TEMPDIR)?>/machines/libraries/focuspoint/img/focuspoint-target.png">
				<img class="target-overlay">
			</div>
		</div>
	</div>	
</div>

<style>
	#<?=$inputID?>{
		width: 100%;
		display: none;
	}
	
	.focuspointURL<?=$inputID?>{
		width: 100%;
		display: block;
	}

	.focuspointContainer{
		display: none;
		margin-top: 3%;
	}

	.focuspoint{
		position: relative;
		width: 80%;
		max-width: 1000px;
		height: 300px;
		overflow: hidden;
	}
</style>
<script>
(function($){
	// SELECTORS
	var $metaWrapper      = $('[data-metaID~=<?=$inputID?>]');
	var $focusPointCon    = $metaWrapper.find('.focuspoint');
	var $focusPointImages = $metaWrapper.find('.focuspoint img');
	var $helperToolImage  = $metaWrapper.find('img.helper-tool-img, img.target-overlay');
	var $focusMarker      = $metaWrapper.find('.reticle')
	var $imgInput         = $('.focuspointURL<?=$inputID?>');
	var $setFocusPoint    = $('.setFocuspoint<?=$inputID?>');
	
	// IF NO JSON SAVE MAKE JSON
	var savedData = ( $.parseJSON($('#<?=$inputID?>').val()) != null )? $.parseJSON($('#<?=$inputID?>').val()) : {};
	var focusPointAttr = {
		w: 0,
		h: 0,
		x: ( savedData['data-focus-x'] == undefined)? 0 : parseFloat(savedData['data-focus-x']),
		y: ( savedData['data-focus-y'] == undefined)? 0 : parseFloat(savedData['data-focus-y']),
		offsetX: ( savedData['lastPositionX'] == undefined)? 50 : parseFloat(savedData['lastPositionX']),
		offsetY: ( savedData['lastPositionY'] == undefined)? 50 : parseFloat(savedData['lastPositionY'])
	};

	$(document).ready(function(){
		// CHECK IF IMAGE URL SET
		if( imgCheck( savedData.imgURL ) ){
			$imgInput.val(savedData.imgURL);
			$metaWrapper.show();
			setImage(savedData.imgURL);
		}

		// SET IMAGE EVENT
		$setFocusPoint.on('click', function(e){
			e.preventDefault();

			if( imgCheck( $imgInput.val() ) ) {
				setImage($imgInput.val());
				$metaWrapper.show();
			} else {
				$metaWrapper.hide();
				printDataAttr();
			}
		});

		// RE-AJUST IMAGE FOCUS EVENT
		$helperToolImage.on('click', function(e){
			var imageW = $(this).width();
			var imageH = $(this).height();

			var offsetX = e.pageX - $(this).offset().left;
			var offsetY = e.pageY - $(this).offset().top;
			var focusX = (offsetX/imageW - 0.5)*2;
			var focusY = (offsetY/imageH - 0.5)*-2;

			focusPointAttr.x = focusX;
			focusPointAttr.y = focusY;

			updateFocusPoint();

			var percentageX = (offsetX/imageW)*100;
			var percentageY = (offsetY/imageH)*100;

			focusPointAttr.offsetX = percentageX.toFixed(0)
			focusPointAttr.offsetY = percentageY.toFixed(0)

			updateMarker();
			printDataAttr();
		});
	});
	
	// CHECK URL EXTENSION IF IMAGE
	function imgCheck(string){
		return (/\.(gif|jpg|jpeg|tiff|png)$/i).test(string);
	}

	// SET IMAGE FOR HELPER
	function setImage(imgURL) {
		
		$("<img/>").attr("src", imgURL).load(function() {
			focusPointAttr.w = this.width;
			focusPointAttr.h = this.height;
			
			$helperToolImage.attr('src', imgURL);
			$focusPointImages.attr('src', imgURL);

			$focusPointCon.data({
				'focusX': focusPointAttr.x,
				'focusY': focusPointAttr.y,
				'imageW': focusPointAttr.w,
				'imageH': focusPointAttr.h
			});
			
			$focusPointCon.focusPoint();
			updateMarker();
			printDataAttr();
		});
	}
	
	// PRINT JSON IN HIDDEN FIELD
	function printDataAttr(){
		finalData = {
			'data-focus-x': focusPointAttr.x.toFixed(2),
			'data-focus-y': focusPointAttr.y.toFixed(2),
			'data-focus-w': parseFloat(focusPointAttr.w),
			'data-focus-h': parseFloat(focusPointAttr.h),
			'lastPositionX': parseFloat(focusPointAttr.offsetX),
			'lastPositionY': parseFloat(focusPointAttr.offsetY),
			'imgURL': _.escape($imgInput.val())
		}

		finalData = JSON.stringify(finalData);
		$('#<?=$inputID?>').val(finalData)
	}

	// RE-AJUST HELPER FOCUS
	function updateFocusPoint(){

		$focusPointCon.data('focusX', focusPointAttr.x);
		$focusPointCon.data('focusY', focusPointAttr.y);
		$focusPointCon.adjustFocus();
	}

	// UPDATE HELPER POSITION
	function updateMarker(x,y){
		$focusMarker.css({
			'top': focusPointAttr.offsetY + '%',
			'left': focusPointAttr.offsetX + '%'
		});
	}
})(jQuery);	
</script>