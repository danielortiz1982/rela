<div class="personItem">
	<a href="#" class="personURL">
		<div class="personPhoto"></div>
		<div class="personInfoBlock">
			<div class="person_board_title"></div>
			<div class="person_name"><span class="person_first_name"></span> <span class="person_last_name"></span></div>
			<div class="person_company"></div>
		</div>
	</a>
</div>