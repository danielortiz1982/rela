<!DOCTYPE html>
<html lang="en" class="<?php echo returnBrowser(); ?>">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?php bloginfo('name');?></title>

    <?php wp_head(); ?>

    <link rel='stylesheet' type='text/css' href='<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.all.css' />
    <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/shadowbox/shadowbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/owlcarousel/owlcarousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/magnific/magnific.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/styles/styles.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

        </head>
        <body data-tempdir="<?php echo PAGEDIR; ?>" id="<?php echo get_post( $post )->post_name; ?>" <?=(returnMobile() == 'true')? 'class="mobileMode"' : 'class="desktopMode"';?>>

            <!-- POPULATE JAVASCRIPT VARIABLES -->
            <div>
                <?php 
                generatePagesJSON(get_the_ID());
                populateJavascript(returnMobile(), 'mobile');

                if(DEVMODE){
                    populateJavascript('true', 'dev_mode');
                } else {
                    populateJavascript('false', 'dev_mode');
                }

                if(isset($_GET['url'])){
                    populateJavascript('true', 'dev_refresh');
                } else {
                    populateJavascript('false', 'dev_refresh');
                }

                if ( is_user_logged_in() ) {
                    populateJavascript('true', 'user_logged_in');

                    $userdat = wp_get_current_user();
                    $allUserData = listUsers('array');

                    foreach ($allUserData as $key => $value) {
                        if($value['id'] == $userdat->data->ID){
                            $userData = $value;
                        }
                    }

                    populateJSON($userData, 'user_data');
                } else {
                    populateJavascript('false', 'user_logged_in');
                }            
                ?>
            </div>

            <!-- HEADER -->
            <nav class="navbar navbar-default navbar-fixed-top header">
                <div class="container">
                    <div class="navbar-header">
                        <div class="displayTable logoContainer">
                            <div class="displayTableCell">
                                <div class="logo-box">
                                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                                        <img src="<?php echo PAGEDIR; ?>/images/graphics/logo.png" alt="<?php bloginfo('name');?>" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="displayTable loginFormWrapper"></div>
                        <div class="toggleContainer">
                            <div class="displayTable">
                                <div class="displayTableCell">
                                    <div>
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="headerNavBox">
                    <div class="container">
                        <?php 

                            class bootstrapDropDown extends Walker_Nav_Menu {
                                function start_lvl( &$output, $depth = 0, $args = array() ) {
                                    $indent = str_repeat("\t", $depth);
                                    $output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
                                }
                            }

                            $defaults = array(
                                'theme_location'  => 'header-menu',
                                'menu'            => '',
                                'container'       => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id'    => 'navbar',
                                'menu_class'      => 'nav navbar-nav',
                                'menu_id'         => '',
                                'echo'            => true,
                                'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '<span>',
                                'link_after'      => '</span>',
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => new bootstrapDropDown()
                                );
                            wp_nav_menu( $defaults );
                        ?>
                    </div>
                </div>
            </nav>

            <!-- PAGE CONTENT -->
            <section class="pageSection"></section>

            <!-- FOOTER -->
            <footer class="footer">
                <div class="container">
                    <div class="footer-content">
                        <div class="footerLinks">
                            <a href="">Sign up for our Newsletter</a>
                        </div>
                        <div class="row socialMedia">
                            <div class="socialMedia-item">
                                <a href="https://www.facebook.com/RealEstateLendersAssociation" target="_blank" class="hyperlink socialMedia-facebook">
                                    <i class="circle"></i>
                                    <i class="icon"></i>
                                </a>
                            </div>
                            <div class="socialMedia-item">
                                <a href="https://www.instagram.com/real_estate_lenders_assoc/" target="_blank" class="hyperlink socialMedia-instagram">
                                    <i class="circle"></i>
                                    <i class="icon"></i>
                                </a>
                            </div>
                            <div class="socialMedia-item">
                                <a href="http://www.linkedin.com/groups?gid=3232841&mostPopular=&trk=tyah" target="_blank" class="hyperlink socialMedia-linkedin">
                                    <i class="circle"></i>
                                    <i class="icon"></i>
                                </a>
                            </div>
                            <div class="socialMedia-item">
                                <a href="https://twitter.com/relassociation" target="_blank" class="hyperlink socialMedia-twitter">
                                    <i class="circle"></i>
                                    <i class="icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="footerInfo">
                            <strong>RELA</strong>
                            <div>
                                <a href="mailto:headquarters@rela.org">Email Us</a> | <a href="http://www.bermangrp.com/" target="_blank" class="hyperlink">Website Created by The Berman Group</a>
                            </div>
                            <div>&#169; <?php echo date('Y'); ?> All Rights Reserved</div>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- SCRIPTS -->
            <div>
                <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                    ga('create', 'UA-72372461-1', 'auto');
                    ga('send', 'pageview');

                </script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/shadowbox/shadowbox.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/modernizr/modernizr.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/backstretch/backstretch.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/boilerplate/boilerplate.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/owlcarousel/owlcarousel.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.min.all.js"></script>
                <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/maplace/maplace.min.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/magnific/magnific.min.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/dotdotdot/dotdotdot.min.js"></script>
                <?php if (returnBrowser() !== "internet-explorer-8"): ?>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/dynamics/dynamics.js"></script>
            <?php endif; ?>
            <?php wp_footer(); ?>
            <script src="<?php echo PAGEDIR; ?>/machines/libraries/fullcalendar/fullcalendar.min.js"></script>
        </div>
    </body>
    </html>