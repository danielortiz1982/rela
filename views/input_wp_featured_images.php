<input type="hidden" class="hidden_meta" id="<?php echo $inputID; ?>" name="<?php echo $inputID; ?>" value='<?php echo $inputValue; ?>' >
<style>
	#<?php echo $inputID; ?>_meta{
		display: none;
	}
	
	.imgContainer{
		height: 100px;
		width: 100px;
		background-position: center;
		display: inline-block;
		margin: 10px;
		position: relative;
		vertical-align: top;
		
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}

	.imgContainer:hover:before{
		content: '';
		background-color: rgba(255, 0, 0, 0.5);
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		cursor: pointer;
	}
	
	.imgContainer:hover:after{
		content: 'Remove';
		color: white;
		position: absolute;
		top: 50%;
		margin-top: -10%;
		left: 50%;
		margin-left: -25%;
		cursor: pointer;
	}
</style>

<div id="<?php echo $inputID;?>Container">
	<div class="open-select-frame button">Choose Images</div>
	
	<h3>Saved Images</h3>
	<div class="savedImages"></div>
	
	<h3>Unsaved Images</h3>
	<div class="unsavedImages"></div>
	
	<ul>
		<li>
			<small>Hold command(Mac) or ctrl(Windows) to select multiples</small>
		</li>
		<li>
			<small>Refresh page to undo changes</small>
		</li>
		<li>
			<small>Click on Image to remove.</small>
		</li>
	</ul>
</div>

<script>
(function($) {
	$(document).ready( function() {
		var container = $('#<?php echo $inputID;?>Container');
		var savedImages = container.find('.savedImages');
		var unsavedImages = container.find('.unsavedImages');
		
		var featuredImagesIds = $('#<?php echo $inputID;?>').val().split(',');
		var featuredImages = [];
		
		var imgContainer = '<div class="imgContainer"/>';

		$.each(featuredImagesIds, function (index, val) {
			var attachment = wp.media.attachment(val);
			
			attachment.fetch().done(function(attachmentData){
				var savedImgContainer = $(imgContainer);
				
				savedImgContainer.css('background-image', 'url(' + attachmentData.sizes.thumbnail.url + ')');
				savedImgContainer.attr('data-id', attachmentData.id);
				
				savedImages.append(savedImgContainer)
			});

			featuredImages.push(attachment)
		});

		$(document).on('click', '.imgContainer', function (e) {
			clickedImg = $(this);

			var cleanFeaturedImages = []
			$.each(featuredImages, function (index, attachment) {
				if( attachment != clickedImg.data('id') ){
					cleanFeaturedImages.push(attachment);
				}
			})
			featuredImages = cleanFeaturedImages;

			var cleanFeaturedImagesIds = []
			$.each(featuredImagesIds, function(index, id) {
				 if( id != clickedImg.data('id') ){
				 	cleanFeaturedImagesIds.push(id);
				 }
			});
			featuredImagesIds = cleanFeaturedImagesIds;
			
			$('#<?php echo $inputID;?>').val( featuredImagesIds.join(',') )

			clickedImg.remove()
		})


		container.find('.open-select-frame').on( 'click', function() {
			// Accepts an optional object hash to override default values.
			
			// var frame = new wp.media.view.MediaFrame.Select({
			window['frame'] = new wp.media.view.MediaFrame.Select({
				// Modal title
				title: 'Select Featured Images',
				// title: '<?php echo $inputID ?>',

				// Enable/disable multiple select
				multiple: true,

				// Library WordPress query arguments.
				library: {
					order: 'ASC',

					// [ 'name', 'author', 'date', 'title', 'modified', 'uploadedTo', 'id', 'post__in', 'menuOrder' ]
					orderby: 'date',

					// mime type. e.g. 'image', 'image/jpeg'
					// type: 'image',

					// Searches the attachment title.
					search: null,

					// Attached to a specific post (ID).
					uploadedTo: null
				},

				button: {
					text: 'Set Featured Images'
				}
			});

			// Fires when the modal opens (becomes visible).
			// @see media.view.Modal.open()
			frame.on( 'open', function() {
				var selection = frame.state().get('selection');
				selection.add(featuredImages)
			} );


			// Fires when a user has selected attachment(s) and clicked the select button.
			// @see media.view.MediaFrame.Post.mainInsertToolbar()
			frame.on( 'select', function() {
				var selectionCollection = frame.state().get('selection');
				featuredImagesIds = [];
				featuredImages = [];
				
				$.each(selectionCollection.models, function (index, attachment) {
					featuredImagesIds.push(attachment.id)
					featuredImages.push(attachment)

					// append if not already set
					if( container.find('[data-id=' + attachment.id + ']').size() == 0 ){
						var selectedImgContainer = $(imgContainer);
						selectedImgContainer.css('background-image', 'url(' + attachment.attributes.sizes.thumbnail.url + ')')
						selectedImgContainer.attr('data-id', attachment.id)

						unsavedImages.append(selectedImgContainer)
					}
				});
				
				$('#<?php echo $inputID;?>').val(featuredImagesIds.join(','))
			} );

			// Get an object representing the current state.
			frame.state();

			// Get an object representing the previous state.
			frame.lastState();

			// Open the modal.
			frame.open();
		});
	});
})(jQuery);
</script>