<div class="pageHome">
	<div class="homeSlideContainer"></div>
	<div class="upcoming-events">
		<div class="container">
			<h3>Upcoming Events</h3>
			<div class="event-listing"></div>
			<h6>
				<a class="internalLink" data-pageid="events" href="/events/">View Calendar</a>
			</h6>
		</div>
	</div>
	<div class="sponsor-listings">
		<div class="container">
			<h3>Sponsors</h3>
			<div class="sponsor-type-listings"></div>
			<div class="sponsor-chapter-listings"></div>
		</div>
	</div>
</div>