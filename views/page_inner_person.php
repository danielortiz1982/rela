<div class="container">
	<h2>NATIONAL BOARD</h2>
	<div class="personPage">
		<div class="personPhotoBlock">
			<div class="personPhoto"></div>
		</div>
		<div class="personInfoBlock">
			<div class="personName"><span class="person_first_name"></span> <span class="person_last_name"></span>, <span class="person_board_type"></span> <span class="person_board_title"></span></div>
			<div class="personCompany"><span class="person_company_title"></span><span class="person_company"></span></div>
			<div class="the_content"></div>
		</div>
	</div>
</div>
