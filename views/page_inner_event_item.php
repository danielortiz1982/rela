<div class="container">
	<h3 class="the_title"></h3>
	<div class="eventText">
		<div class="the_content"></div>
	</div>
	<div class="eventInfo">
		<div class="eventTime">
			<div class="bold">Date</div> 
		</div>
		<div class="eventLocation">
			<span class="bold">Location</span>
			<div class="event_location"></div>
		</div>
		<div class="eventAddress">
			<span class="bold">Address</span>
			<div>
				<span class="event_address1"></span> <span class="event_address2"></span>
			</div>
			<div>
				<span class="event_city"></span>, <span class="event_state"></span> <span class="event_zip_code"></span>
			</div>
		</div>
		<div class="eventLink">
			<a href="" target="_blank" class="hyperlink eventRegister">Register Here ></a>
		</div>
	</div>
</div>