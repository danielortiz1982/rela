(function($){

	$(document).ready(function(){

		webApp = new boilerplate('webApp');

		if(!webApp.ajaxer){
			webApp.start({
				singlePage: false,
				defaultPage: 'home',
				RewriteBase: "/",
				themeFolderName: 'rela',
				typekitID: 'mqy4viv',
				filesToVariablesArray: [
				{'page_home': 'views/page_home.php'},
				{'page_inner': 'views/page_inner.php'},
				{'page_inner_secondary_editor': 'views/page_inner_secondary_editor.php'},
				{'page_inner_person': 'views/page_inner_person.php'},
				{'page_inner_chapters': 'views/page_inner_chapters.php'},
				{'page_inner_chapter': 'views/page_inner_chapter.php'},
				{'page_inner_events': 'views/page_inner_events.php'},
				{'page_inner_event_item': 'views/page_inner_event_item.php'},
				{'page_inner_no_title': 'views/page_inner_no_title.php'},
				{'slide_container': 'views/output_slide_container.php'},
				{'chapter_name_item': 'views/output_chapter_name_item.php'},
				{'chapter_title_item': 'views/output_chapter_title_item.php'},
				{'event_item': 'views/output_event_item.php'},
				{'sponsor_type_item': 'views/output_sponsor_type_item.php'},
				{'sponsor_chapter_item': 'views/output_sponsor_chapter_item.php'},
				{'sponsor_item': 'views/output_sponsor_item.php'},
				{'board_type_item': 'views/output_board_type_item.php'},
				{'person_item': 'views/output_person_item.php'},
				{'chapter_person_item': 'views/output_chapter_person_item.php'},
				{'news_item': 'views/output_news_item.php'},
				{'breaking_news': 'views/output_breaking_news.php'},
				{'chapter_dues_item': 'views/output_chapter_dues_item.php'},
				{'chapter_link_item': 'views/output_chapter_link_item.php'},
				{'chapter_subnav_item': 'views/output_chapter_subnav_item.php'},
				{'chapter_colors_item': 'views/output_chapter_colors_item.php'},
				{'event_photos_item': 'views/output_event_photos_item.php'},
				{'blog_item': 'views/output_blog_item.php'},
				{'login_form': 'views/output_login_form.php'}
				],
				viewRender: {
					"home": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							pageObject = $(that.php_page_home);
							$('.pageSection').html(pageObject);

							pageObject.find('.internalLink').on('click', function(e){
								e.preventDefault();
								that.pushPageNav($(this).attr('data-pageid'));
							});

							that.bs_slideShow({
								target: pageObject.find('.homeSlideContainer'),
								callback: function (argument) {
									pageObject.find('.homeSlideContainer').append(pageObject.find('.slideButtons'))
								}
							});

							that.buildEventListing($('.pageSection').find('.upcoming-events'), pageID);

							that.buildSponsorTypeListing($('.pageSection').find('.sponsor-type-listings'), pageID);
							that.buildSponsorChapterListing($('.pageSection').find('.sponsor-chapter-listings'), pageID);

							that.changePage();
						});
					},
					"national-board": function(thisThat){
						var that = thisThat;
						if(that.postIDnotSet()){
							that.returnAndSaveJsonData('listPages', function(pagesData){
								thisPageData = that.accessor_listPages[pageID];
								$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));

								if($('.slideShowContainer').size() == 0){
									slideShowContainer = $('<div class="slideShowContainer" />')
									$('.pageSection').before(slideShowContainer)
								}

								that.bs_slideShow({
									target: $('.slideShowContainer'),
									slideContent: false,
									slideControls: false,
									slideButtons: false,
									slideData: pageID
								});

								// create sub nav container and sub nav
								if($('.subNavContainer').size() == 0){
									subNavContainer = $('<div class="subNavContainer" />')
									$('.pageSection').before(subNavContainer)
								}
								that.buildSubNav($('.subNavContainer'));	

								if($('.nationalBoardContainer').size() == 0){
									nationalBoardContainer = $('<div class="nationalBoardContainer" />')
									$('.pageSection').find('.container').append(nationalBoardContainer)
								}
								that.buildNationalBoardListing($('.pageSection').find('.nationalBoardContainer'), pageID);	

								that.changePage();
							});
						} else {
						that.returnAndSaveJsonData('listPeople', function(peopleData){
							_.each(peopleData, function(value_peopleData, index_peopleData){
								if (that.slugify(value_peopleData.the_title) == postID ) {
									$('.pageSection').html(that.renderModel(value_peopleData, $(that.php_page_inner_person)));					

									if($('.slideShowContainer').size() == 0){
										slideShowContainer = $('<div class="slideShowContainer" />')
										$('.pageSection').before(slideShowContainer)
									}

									that.bs_slideShow({
										target: $('.slideShowContainer'),
										slideContent: false,
										slideControls: false,
										slideButtons: false,
										slideData: pageID
									});

								// create sub nav container and sub nav
								if($('.subNavContainer').size() == 0){
									subNavContainer = $('<div class="subNavContainer" />')
									$('.pageSection').before(subNavContainer)
								}
								that.buildSubNav($('.subNavContainer'));

								if(value_peopleData.featuredImage != null){
									$('.pageSection').find('.personPhoto').backstretch(value_peopleData.featuredImage)
								}
								
								$('.pageSection').find('.person_board_type').addClass('boardTypeID' + value_peopleData.person_board_type)

								if(value_peopleData.person_company_title  != ''){
									$('.pageSection').find('.person_company_title').html(value_peopleData.person_company_title + ", ")
									} else {
									$('.pageSection').find('.person_company_title').remove();
									}
								}
							})	

							that.returnAndSaveJsonData('listBoardTypes', function(boardTypesData){
								_.each(boardTypesData, function (value_boardTypesData, index_boardTypesData) {
									$('.pageSection').find('.boardTypeID' + value_boardTypesData.post_id).html(value_boardTypesData.the_title);
								})	
							});

							that.changePage();
							});
						}
					},
					"rela-in-the-news": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));

							if($('.slideShowContainer').size() == 0){
								slideShowContainer = $('<div class="slideShowContainer" />')
								$('.pageSection').before(slideShowContainer)
							}

							that.bs_slideShow({
								target: $('.slideShowContainer'),
								slideContent: false,
								slideControls: false,
								slideButtons: false,
								slideData: pageID
							});

							// create sub nav container and sub nav
							if($('.subNavContainer').size() == 0){
								subNavContainer = $('<div class="subNavContainer" />')
								$('.pageSection').before(subNavContainer)
							}
							that.buildSubNav($('.subNavContainer'));

							if($('.newsItemsContainer').size() == 0){
								newsItemsContainer = $('<div class="newsItemsContainer" />')
								$('.pageSection').find('.container').find('.the_content').append(newsItemsContainer)
							}
							that.buildNewsItemsListing($('.pageSection').find('.newsItemsContainer'), pageID);

							that.changePage();
						});
					},	
					"breaking-news": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));

							if($('.slideShowContainer').size() == 0){
								slideShowContainer = $('<div class="slideShowContainer" />')
								$('.pageSection').before(slideShowContainer)
							}

							that.bs_slideShow({
								target: $('.slideShowContainer'),
								slideContent: false,
								slideControls: false,
								slideButtons: false,
								slideData: pageID
							});

							// create sub nav container and sub nav
							if($('.subNavContainer').size() == 0){
								subNavContainer = $('<div class="subNavContainer" />')
								$('.pageSection').before(subNavContainer)
							}
							that.buildSubNav($('.subNavContainer'));

							$('.pageSection').find('.container').append(that.php_breaking_news);

							that.changePage();
						});
					},
					"membership": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner_secondary_editor)));

							if($('.slideShowContainer').size() == 0){
								slideShowContainer = $('<div class="slideShowContainer" />')
								$('.pageSection').before(slideShowContainer)
							}

							that.bs_slideShow({
								target: $('.slideShowContainer'),
								slideContent: false,
								slideControls: false,
								slideButtons: false,
								slideData: pageID
							});

							// create sub nav container and sub nav
							if($('.subNavContainer').size() == 0){
								subNavContainer = $('<div class="subNavContainer" />')
								$('.pageSection').before(subNavContainer)
							}
							that.buildSubNav($('.subNavContainer'));

							if($('.chapterDuesContainer').size() == 0){
								chapterDuesContainer = $('<div class="chapterDuesContainer" />')
								$('.pageSection').find('.secondary_editor').before(chapterDuesContainer)
							}
							that.buildChapterDuesListing($('.pageSection').find('.chapterDuesContainer'), pageID);

							that.changePage();
						});
					},		
					"chapters": function(thisThat){
				        var that = thisThat;
						if(that.postIDnotSet()){
							that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner_chapters)));
							$('.pageSection').find('.secondary_editor').html(_.unescape(thisPageData.secondary_editor));

							if($('.slideShowContainer').size() == 0){
								slideShowContainer = $('<div class="slideShowContainer" />')
								$('.pageSection').before(slideShowContainer)
							}

							that.bs_slideShow({
								target: $('.slideShowContainer'),
								slideContent: false,
								slideControls: false,
								slideButtons: false,
								slideData: pageID
							});

							// create sub nav container and sub nav
							if($('.subNavContainer').size() == 0){
								subNavContainer = $('<div class="subNavContainer" />')
								$('.pageSection').before(subNavContainer)
							}
							that.buildSubNav($('.subNavContainer'));

							if($('.chapterLinksContainer').size() == 0){
								chapterLinksContainer = $('<div class="chapterLinksContainer" />')
								$('.pageSection').find('.secondaryContainer').before(chapterLinksContainer)
							}
							that.buildChapterLinksListing($('.pageSection').find('.chapterLinksContainer'));

							that.changePage();
						});

						} else {
							that.returnAndSaveJsonData('listChapters', function(chaptersData){

								// create featured image container and featured image
								if($('.slideShowContainer').size() == 0){
									chapterFeaturedImageContainer = $('<div class="slideShowContainer" />')
									$('.pageSection').before(chapterFeaturedImageContainer)
									}
								that.buildChapterFeaturedImage(chaptersData, $('.slideShowContainer'));	

								// get this chapters data and add to DOM
								var thisChapter = _.find(chaptersData, function(chapterData) {
									return postID == that.slugify(chapterData.the_title);
									});
								chaptersObject = that.renderModel(thisChapter, $(that.php_page_inner_chapter));	

								$('.pageSection').html(chaptersObject);	

								if(thisChapter.chapter_join_url  != ''){
									chaptersObject.find('.joinLink a').attr('href', thisChapter.chapter_join_url)
								} else {
									chaptersObject.find('.joinLink').html("");
								}

								if(thisChapter.chapter_sponsor_url  != ''){
									chaptersObject.find('.sponsorsLink a').attr('href', thisChapter.chapter_sponsor_url)
								} else {
									chaptersObject.find('.sponsorsLink').html("");
								}	

								that.buildChapterOfficersListing($('.pageSection').find('.chapterOfficersContainer'));

								that.buildChapterSponsorListing($('.pageSection').find('.sponsor-chapter-listings'), pageID);	

								that.changePage();
							});
							that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
								// create sub nav container and sub nav
								if($('.subNavContainer').size() == 0){
									subNavContainer = $('<div class="subNavContainer" />')
									$('.pageSection').before(subNavContainer)
								}
								that.buildSubNav($('.subNavContainer'));
							});
						}
					},
					"events": function(thisThat){
						var that = thisThat;
						if(that.postIDnotSet()){
							that.returnAndSaveJsonData('listPages', function(pagesData){
								thisPageData = that.accessor_listPages[pageID];
								$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner_events)));	

								if($('.slideShowContainer').size() == 0){
									slideShowContainer = $('<div class="slideShowContainer" />')
									$('.pageSection').before(slideShowContainer)
								}	

								that.bs_slideShow({
									target: $('.slideShowContainer'),
									slideContent: false,
									slideControls: false,
									slideButtons: false,
									slideData: pageID
								});	

								that.buildChapterColorsListing($('.pageSection').find('.chapterColorsContainer'));

								that.renderEventsCalendar( $('.pageSection').find('.the_content'));	

								that.changePage();
							});
						} else {
							that.returnAndSaveJsonData('listPages', function(pagesData){
								thisPageData = that.accessor_listPages[pageID];

								if($('.slideShowContainer').size() == 0){
									slideShowContainer = $('<div class="slideShowContainer" />')
									$('.pageSection').before(slideShowContainer)
								}	

								that.bs_slideShow({
									target: $('.slideShowContainer'),
									slideContent: false,
									slideControls: false,
									slideButtons: false,
									slideData: pageID
								});	
							});

							that.returnAndSaveJsonData('listEvents', function(eventsData){
								var thisEvent = _.find(eventsData, function(eventData) {
									return postID == that.slugify(eventData.the_title + "-" + eventData.event_start_date);
								});

								if(typeof thisEvent !== "undefined"){
									eventObject = that.renderModel(thisEvent, $(that.php_page_inner_event_item));	
									$('.pageSection').html(eventObject);	

									eventObject.find('.eventTime').append(that.returnFormattedTimeRange(thisEvent.event_start_date, thisEvent.event_end_date));

									if(thisEvent.event_url != "") {
										eventObject.find('.eventRegister').attr('href', thisEvent.event_url);
									} else {
										eventObject.find('.eventLink').remove();
									}

									if(thisEvent.event_location == "") {
										eventObject.find('.eventLocation').remove();
									}

									if(thisEvent.event_state == "") {
										eventObject.find('.eventAddress').remove();
									}

									if(thisEvent.event_nyc_young_member_feature != "") {
										eventObject.find('.the_content').append('<p>*NYC young member event</p>');
									}

								}


								that.changePage();
							});
						}
					},
					"gallery": function(thisThat){
						var that = thisThat;
						if(that.postIDnotSet()){
							that.returnAndSaveJsonData('listPages', function(pagesData){
								that.returnAndSaveJsonData('listEvents', function(eventsData){
									thisPageData = that.accessor_listPages[pageID];
									$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner_no_title)));

									$('.pageSection').find('.the_content').append('<div class="itemsContainer"/>');

									eventsData = _.sortBy(eventsData, 'event_start_date');
									eventsData.reverse();

									var imageArrayRequest = [];

									_.each(eventsData, function(value_events, index_events) {
										if(moment(value_events.event_start_date, "X").tz("America/New_York").format("X") < moment().tz("America/New_York").format("X") ) {
											if(that.accessor_listEvents[value_events.post_id].events_event_gallery_images != ""){
												eventsObject = that.renderModel(value_events, $(that.php_event_photos_item));
												eventsObject.find('a').attr('href', '/gallery/' + that.slugify(_.unescape(value_events.the_title)) + '/');
												eventsObject.find('a').addClass('eventPhotosURL');
												eventsObject.find('a').data('pageid', 'gallery');
												eventsObject.find('a').data('postid', that.slugify(_.unescape(value_events.the_title)));
												eventsObject.find('.eventFeaturedImage').addClass('eventID' + value_events.post_id);	
												$('.itemsContainer').append(eventsObject);
												var image_to_request = value_events.events_event_gallery_images.split(',')[0];
												eventsObject.find('.eventFeaturedImage').addClass('imageItem' + image_to_request)
												imageArrayRequest.push(image_to_request);

												// if(value_events.featuredImage == null){
												// 	var imageArrayRequest = value_events.events_event_gallery_images.split(',');
												// 	$.post(that.pageDir + "/machines/handlers/loadPost.php", {postRequest: imageArrayRequest}, function(featuredImagesData){
												// 		firstFeaturedImage = [];
												// 		firstFeaturedImage.push(featuredImagesData[imageArrayRequest[0]])
												// 		_.each(firstFeaturedImage, function(value_firstFeaturedImage, index_firstFeaturedImage){
												// 			$('.itemsContainer').find('.eventID' + value_events.post_id).backstretch(value_firstFeaturedImage.image_data.medium);
												// 		});
												// 	}, 'json');	
												// } else {
												// 	eventsObject.find('.eventFeaturedImage').backstretch(value_events.featuredImage);
												// }
											}
										}
									});

									$.post(that.pageDir + "/machines/handlers/loadPost.php", {postRequest: imageArrayRequest}, function(featuredImagesData){
										_.each(featuredImagesData, function (value_featuredImagesData, index_featuredImagesData) {
											var image_target = $('.imageItem' + index_featuredImagesData);
											var image_url = value_featuredImagesData.image_data.medium;
											image_target.backstretch(image_url)
										})
									}, 'json');
									

									$('.eventPhotosURL').off('click');
									$('.eventPhotosURL').on('click', function(e){
										e.preventDefault();
										that.pushPageNav($(this).data('pageid'), $(this).data('postid'));
									})

									that.changePage();
								});
							});
						} else {
							that.returnAndSaveJsonData('listEvents', function(eventsData){
								_.each(eventsData, function(value_events, index_events){
									if (that.slugify(value_events.the_title) == postID ) {
										$('.pageSection').html(that.renderModel(value_events, $(that.php_page_inner)));	

										$('.pageSection').find('.the_content').html("");

										postRequest = value_events.events_event_gallery_images.split(',');
										$.post(that.pageDir + "/machines/handlers/loadPost.php", {postRequest: postRequest}, function(featuredImagesData){
											featuredImages = [];
											_.each(postRequest, function(value_post, index_post){
												featuredImages.push(featuredImagesData[value_post])
											});

											_.each(featuredImages, function(value_featuredImages, index_featuredImages){
												returnLink = $('<a class="galleryImageLink" />');
												returnItem = $('<div class="galleryImage" />');
												returnLink.attr('href', value_featuredImages.image_data.full)
												returnLink.append(returnItem);
												$('.pageSection').find('.the_content').append(returnLink);
												if(value_featuredImages.image_data.thumb != null){
													returnItem.backstretch(value_featuredImages.image_data.thumb);
												}
												
												$('.galleryImageLink').magnificPopup({
													gallery: {
														enabled: true
													},
													  type: 'image'
												});
											});
										}, 'json');		
									}
								})		

								that.changePage();
							});
						}
					},
					"contact": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));

							if($('.bannerMap').size() == 0){
								bannerMapContainer = $('<div class="bannerMap" />')
								$('.pageSection').before(bannerMapContainer)
							}
							
							new Maplace({
								map_div: ".bannerMap",
								locations: [{
									lat: 40.751080, 
									lon: -73.976470,
									zoom: 16
								}],
								controls_on_map: false
							}).Load();
							
							that.changePage();
						});
					},
					"default": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));

							if($('.slideShowContainer').size() == 0){
								slideShowContainer = $('<div class="slideShowContainer" />')
								$('.pageSection').before(slideShowContainer)
							}

							that.bs_slideShow({
								target: $('.slideShowContainer'),
								slideContent: false,
								slideControls: false,
								slideButtons: false,
								slideData: pageID
							});

							// create sub nav container and sub nav
							if($('.subNavContainer').size() == 0){
								subNavContainer = $('<div class="subNavContainer" />')
								$('.pageSection').before(subNavContainer)
							}
							that.buildSubNav($('.subNavContainer'));

							that.changePage();
						});
					}
					// FOR BLOG RENDERING
					// "blog": function(thisThat){
					// 	var that = thisThat;
					// 	that.renderBlog();
					// }
					// FOR SINGLE PAGE RENDERING
					// "default": function(thisThat, pageIDrequest){
					// 	var that = thisThat;
					// 	that.returnAndSaveJsonData('listPages', function(pagesData){
					// 		thisPageData = that.accessor_listPages[pageIDrequest];
					// 		pageObject = that.renderModel(thisPageData, $(that.php_page_inner));
					// 		pageObject.attr('id', 'page_' + pageIDrequest)
					// 		$('.pageSection').append(pageObject);
					// 	});
					// }
				},
				helpers: {
					bs_slideShow: function(params){
						var that = this;
						var slideTimer;
						var slideData;
						var slideIncrement = -1;
						var slideTransitionTime = 6000;
						var sleepTime = 12000;
						var fadeTiming = 400;
						var sleepTimeout;

						var show_slideControls = (typeof params.slideControls === "undefined") ? true : params.slideControls;
						var show_slideContent = (typeof params.slideContent === "undefined") ? true : params.slideContent;
						var show_slideButtons = (typeof params.slideButtons === "undefined") ? true : params.slideButtons;
						var slideDataRequest = (typeof params.slideData === "undefined") ? null : params.slideData;
						var renderCallback = (typeof params.callback === "undefined") ? function(){} : params.callback;

						destroyInstances();
						getData();

						function destroyInstances(){
							if(typeof that.slideShowInstances === "undefined"){
								that.slideShowInstances = [];
							}

							_.each(that.slideShowInstances, function(value_slideShowInstances, index_slideShowInstances){
								value_slideShowInstances.stop()
								that.slideShowInstances.splice(index_slideShowInstances, 1);
							});

							that.slideShowInstances.push({
								stop: stopSlideShow
							})
						}

						function renderSlideShowDom(){
							var slideDOM = $(that.php_slide_container);
							var slideButtonClone = slideDOM.find('.slideButton').clone();

							slideDOM.find('.slideButton').remove();

							_.each(slideData, function(value_slideData, index_slideData){
								var thisSlideButton = slideButtonClone.clone();
								thisSlideButton.attr('data-index', index_slideData)
								thisSlideButton.attr('data-postid', value_slideData.post_id)
								slideDOM.find('.slideButtons').append(thisSlideButton)
							});

							params.target.append(slideDOM);
							slideDOM.addClass('bs_slideshow');

							if(!show_slideControls){
								slideDOM.find('.slideControls').css('display', 'none')
							}

							if(!show_slideContent){
								slideDOM.find('.slideContent').css('display', 'none')
							}

							if(!show_slideButtons){
								slideDOM.find('.slideButtons').css('display', 'none')
							}
						}
						function getData(){
							if(slideDataRequest == null){
								that.returnAndSaveJsonData('listHomeSliders', function(homeSlidersData){
									slideData = homeSlidersData;
									renderSlideShowDom();
									bindEvents();
									startSlideShow();
								});
							} else {
								that.returnAndSaveJsonData('listPages', function(pagesData){
									if(that.accessor_listPages[slideDataRequest].page_featured_images == ""){
										that.returnAndSaveJsonData('listHomeSliders', function(homeSlidersData){
											slideData = homeSlidersData;
											renderSlideShowDom();
											bindEvents();
											startSlideShow();
										});
									} else {
										var imageArrayRequest = that.accessor_listPages[slideDataRequest].page_featured_images.split(',')
										$.post(that.pageDir + "/machines/handlers/loadPost.php", { postRequest: imageArrayRequest}, function(imageData){
											thisPageSlideData = [];

											_.each(imageArrayRequest, function(value_imageArrayRequest, index_imageArrayRequest){
												value_imageData = imageData[value_imageArrayRequest]
												thisSlideData = value_imageData;
												thisSlideData['featuredImage'] = value_imageData.image_data.full;
												thisPageSlideData.push(thisSlideData)
											})

											slideData = thisPageSlideData;
											renderSlideShowDom();
											bindEvents();
											startSlideShow();											
										}, 'json');
									}
								})
							}
						}						

						function startSlideShow(){
							renderCallback()
							changeSlide();
							slideTimer = setInterval(function(){
								changeSlide();
							}, slideTransitionTime);
						}						

						function stopSlideShow(){
							clearInterval(slideTimer);
						}						

						function changeSlide(slide_action){
							if(typeof slide_action === "undefined" || slide_action == 'next'){
								nextSlide();
							} else if (slide_action == 'prev') {
								prevSlide();
							} else {
								goToSlide(slide_action);
							}						

							function goToSlide(index){
								slideIncrement = index;
								renderSlide(slideData[slideIncrement]);
							}						

							function nextSlide(){
								if(slideIncrement+1 < _.size(slideData)){
									slideIncrement++;
									renderSlide(slideData[slideIncrement]);
								} else {
									slideIncrement = 0;
									renderSlide(slideData[slideIncrement]);
								}
							}						

							function prevSlide(){
								if(slideIncrement-1 >= 0){
									slideIncrement--;
									renderSlide(slideData[slideIncrement]);
								} else {
									slideIncrement = _.size(slideData) - 1;
									renderSlide(slideData[slideIncrement]);
								}
							}
						}
						function renderSlide(slideData){
							params.target.backstretch(slideData.featuredImage, {
								fade: fadeTiming
							});						

							that.renderModel(slideData, params.target);
							params.target.find('.slideButton').removeClass('current')
							params.target.find('.slideButton[data-postid="' + slideData.post_id + '"]').addClass('current')						

							if(typeof that.accessor_id_listPages[slideData.home_slide_page_link] !== "undefined"){
								var thisPageRequest = that.slugify(that.accessor_id_listPages[slideData.home_slide_page_link].the_title);
								params.target.find('.home_slide_link_text').attr('data-pageid', thisPageRequest)
								// params.target.find('.the_content').dotdotdot({
								// 	height: 290
								// })
							}
							
						}
						function bindEvents(){
							params.target.find('.slideControl').off('click')
							params.target.find('.slideControl').on('click', function(e){
								e.preventDefault();
								stopSlideShow();						

								clearTimeout(sleepTimeout)
								sleepTimeout = setTimeout(function(){
									startSlideShow();
								}, sleepTime);						

								if($(this).attr('data-control') == 'next'){
									changeSlide('next')
								} else {
									changeSlide('prev')
								}
							})						

							params.target.find('.slideButton').off('click')
							params.target.find('.slideButton').on('click', function(e){
								e.preventDefault();
								stopSlideShow();						

								clearTimeout(sleepTimeout)
								sleepTimeout = setTimeout(function(){
									startSlideShow();
								}, sleepTime);						

								changeSlide($(this).attr('data-index'))
							});						

							params.target.find('.home_slide_link_text').off('click')
							params.target.find('.home_slide_link_text').on('click', function(e){
								e.preventDefault();
								stopSlideShow();						
								that.pushPageNav($(this).attr('data-pageid'))
								});						
							}
					},
					buildEventListing: function(target, mode){
						var that = this;
						recentEventsCounter = 0;

						that.returnAndSaveJsonData('listEvents', function(eventsData){
							_.each(eventsData, function (value_eventsData, index_eventsData) {
								if(moment(value_eventsData.event_start_date, "X").format("X") > moment().format("X") && recentEventsCounter < 3) {
									recentEventsCounter++;
									returnObject = that.renderModel(value_eventsData, $(that.php_event_item));
									eventDate = moment(value_eventsData.event_start_date, "X").tz("America/New_York").format('MMMM D');
									returnObject.find('.eventChapter').addClass('chapterItem' + value_eventsData.event_chapter)
									returnObject.find('.eventDate').html(eventDate)
									thisPageId = 'events';
									thisPostId = that.slugify(_.unescape(value_eventsData.the_title + "-" + value_eventsData.event_start_date));
									returnObject.find('.eventLink a').attr('data-pageid', thisPageId)
									returnObject.find('.eventLink a').attr('data-postid', thisPostId)
									returnObject.find('.eventLink a').attr('href', '/' + thisPageId + '/' + thisPostId + '/')
									target.find('.event-listing').append(returnObject);
								}
							})
							$('.eventLink').find('a').off('click');
							$('.eventLink').find('a').on('click', function(e){
								e.preventDefault();
								that.pushPageNav($(this).attr('data-pageid'), $(this).attr('data-postid'))
								}		
							)
							that.returnAndSaveJsonData('listChapters', function(chaptersData){
								_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
									returnObject = that.renderModel(value_chaptersData, $(that.php_chapter_name_item));
									target.find('.event-listing').find('.chapterItem' + value_chaptersData.post_id).append(returnObject);
									target.find('.event-listing').find('.chapterItem' + value_chaptersData.post_id).css('color', value_chaptersData.chapter_color)
								})
							});
							if(target.find('.event-listing').html() == ""){
								target.remove();
							}
							target.find('.the_content').dotdotdot({
								height: 67
							})
						});
					},
					buildSponsorTypeListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listSponsorTypes', function(sponsorTypesData){
							_.each(sponsorTypesData, function (value_sponsorTypesData, index_sponsorTypesData) {
								returnObject = that.renderModel(value_sponsorTypesData, $(that.php_sponsor_type_item));
								returnObject.addClass('sponsorTypeItem' + value_sponsorTypesData.post_id)
								target.append(returnObject);
							})	
							that.returnAndSaveJsonData('listSponsors', function(sponsorsData){
								_.each(sponsorsData, function (value_sponsorsData, index_sponsorsData) {
									returnObject = that.renderModel(value_sponsorsData, $(that.php_sponsor_item));
									if(value_sponsorsData.sponsor_url  != ''){
										returnObject.find('.sponsorLogo').wrap('<a href=' + value_sponsorsData.sponsor_url + ' target="_blank"></a>');
									} else {
										returnObject.find('.sponsorLogo').wrap('<div class="sponsorItemWrapper"></div>');
									}
									if(value_sponsorsData.sponsor_type  != ''){
										target.find('.sponsorTypeItem' + value_sponsorsData.sponsor_type).find('.sponsorCarousel').append(returnObject)
									}
									if(value_sponsorsData.featuredImage != null){
										returnObject.find('.sponsorLogo').backstretch(value_sponsorsData.featuredImage)
									}
								})	
					
								target.find('.sponsorCarousel').each(function() {
									var $that = $(this);
									if ($that.find('.sponsorItem').length > 1) { 
										$that.owlCarousel({
											items: 1,
											loop: true,
											pagination: true,
											autoplay: true,
											autoplayTimeout: 5000,
											autoplayHoverPause: true,
										})
									}
								});

								target.find('.sponsorCarousel').each(function(){
									if($(this).html() == ""){
										$(this).parents('.sponsorTypeItem').remove()
									}
								});
							});
						});
					},
					buildSponsorChapterListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								returnObject = that.renderModel(value_chaptersData, $(that.php_sponsor_chapter_item));
								returnObject.addClass('chapterItem' + value_chaptersData.post_id)
								target.append(returnObject);
							})	
							that.returnAndSaveJsonData('listSponsors', function(sponsorsData){
								_.each(sponsorsData, function (value_sponsorsData, index_sponsorsData) {
									returnObject = that.renderModel(value_sponsorsData, $(that.php_sponsor_item));
									if(value_sponsorsData.sponsor_url  != ''){
										returnObject.find('.sponsorLogo').wrap('<a href=' + value_sponsorsData.sponsor_url + ' target="_blank"></a>');
									} else {
										returnObject.find('.sponsorLogo').wrap('<div class="sponsorItemWrapper"></div>');
									}
									if(value_sponsorsData.sponsor_chapter  != ''){
										target.find('.chapterItem' + value_sponsorsData.sponsor_chapter).find('.sponsorCarousel').append(returnObject)
									}
									if(value_sponsorsData.featuredImage != null){
										returnObject.find('.sponsorLogo').backstretch(value_sponsorsData.featuredImage)
									}
								})	
								
								target.find('.sponsorCarousel').each(function() {
									var $that = $(this);
									if ($that.find('.sponsorItem').length > 1) { 
										$that.owlCarousel({
											items: 1,
											loop: true,
											pagination: true,
											autoplay: true,
											autoplayTimeout: 5000,
											autoplayHoverPause: true,
										})
									}
								});

								target.find('.sponsorCarousel').each(function(){
									if($(this).html() == ""){
										$(this).parents('.sponsorChapterItem').remove()
									}
								});
							});
						});
					},
					buildNationalBoardListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listBoardTypes', function(boardTypesData){
							_.each(boardTypesData, function (value_boardTypesData, index_boardTypesData) {
								returnObject = that.renderModel(value_boardTypesData, $(that.php_board_type_item));
								returnObject.addClass('boardTypeItem' + value_boardTypesData.post_id)
								target.append(returnObject);
							})	

							that.returnAndSaveJsonData('listPeople', function(peopleData){
								_.each(peopleData, function (value_peopleData, index_peopleData) {
									returnObject = that.renderModel(value_peopleData, $(that.php_person_item));
									returnObject.find('a').attr('href', '/national-board/' + that.slugify(_.unescape(value_peopleData.the_title)) + '/');
									returnObject.find('a').data('pageid', 'national-board');
									returnObject.find('a').data('postid', that.slugify(_.unescape(value_peopleData.the_title)));
									if(value_peopleData.person_board_type  != ''){
										target.find('.boardTypeItem' + value_peopleData.person_board_type).find('.peopleContainer').append(returnObject)
									}
									if(value_peopleData.featuredImage != null){
										returnObject.find('.personPhoto').backstretch(value_peopleData.featuredImage)
									}
								})	
								
								target.find('.peopleContainer').each(function(){
									if($(this).html() == ""){
										$(this).parents('.boardTypeItem').remove()
									}
								});

								$('.personURL').off('click');
								$('.personURL').on('click', function(e){
									e.preventDefault();
									that.pushPageNav($(this).data('pageid'), $(this).data('postid'));
								})	

							});
						});
					},
					buildNewsItemsListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listNewsItems', function(newsItemsData){
							newsItemsData = _.sortBy(newsItemsData, 'news_item_date').reverse();
							_.each(newsItemsData, function (value_newsItemsData, index_newsItemsData) {
								returnObject = that.renderModel(value_newsItemsData, $(that.php_news_item));
								newsItemDate = moment(value_newsItemsData.news_item_date, "X").tz("America/New_York").format('MMMM Do, YYYY');
								returnObject.find('.newsItemDate').html(newsItemDate)
								if(value_newsItemsData.news_item_url  != ''){
									returnObject.find('a').attr('href', value_newsItemsData.news_item_url);
								} else {
									returnObject.find('.newsItemDate').unwrap();
								}
								target.append(returnObject);
							})	
						})		
					},
					buildChapterFeaturedImage: function(chaptersData, target){
						var that = this;
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								if (that.slugify(_.unescape(value_chaptersData.the_title)) == postID ){
									if(value_chaptersData.featuredImage != null){
										target.backstretch(value_chaptersData.featuredImage);
									} else {
										target.find('div').remove();
									}
								}
							});
						});
					},
					buildChapterDuesListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								returnObject = that.renderModel(value_chaptersData, $(that.php_chapter_dues_item));
								target.append(returnObject);
							})	
						})		
					},
					buildChapterLinksListing: function(target){
						var that = this;
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								returnObject = that.renderModel(value_chaptersData, $(that.php_chapter_link_item));
								thisPageId = 'chapters';
								thisPostId = that.slugify(_.unescape(value_chaptersData.the_title));
								returnObject.find('a').attr('data-pageid', thisPageId)
								returnObject.find('a').attr('data-postid', thisPostId)
								returnObject.find('a').attr('href', '/' + thisPageId + '/' + thisPostId + '/')
								if(value_chaptersData.featuredImage != null){
									returnObject.find('.chapterLinkPhoto').backstretch(value_chaptersData.featuredImage)
									} else {
									returnObject.find('a').remove();
								}
								target.append(returnObject);
							})	
							$('.chapterLinksContainer').find('a').off('click');
							$('.chapterLinksContainer').find('a').on('click', function(e){
								e.preventDefault();	
								that.pushPageNav($(this).attr('data-pageid'), $(this).attr('data-postid'))
							})
						})
					},
					buildChapterOfficersListing: function(target){
						var that = this;

						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								if (that.slugify(_.unescape(value_chaptersData.the_title)) == postID ){
									thisPostId = value_chaptersData.post_id
								}
							});

							that.returnAndSaveJsonData('listChapterTitles', function(chapterTitlesData){
								_.each(chapterTitlesData, function (value_chapterTitlesData, index_chapterTitlesData) {
									returnObject = that.renderModel(value_chapterTitlesData, $(that.php_chapter_title_item));
									returnObject.addClass('chapterTitleID' + value_chapterTitlesData.post_id)
									target.append(returnObject);
								})	

								that.returnAndSaveJsonData('listPeople', function(peopleData){
									_.each(peopleData, function (value_peopleData, index_peopleData) {
										if(value_peopleData.person_chapter  == thisPostId){
											returnObject = that.renderModel(value_peopleData, $(that.php_chapter_person_item));
											if(value_peopleData.person_chapter_title  != ''){
												target.find('.chapterTitleID' + value_peopleData.person_chapter_title).find('.peopleContainer').append(returnObject)
											}
										}
									})	

									target.find('.peopleContainer').each(function(){
										if($(this).html() == ""){
											$(this).parents('.chapterTitleItem').remove()
										}
									});
								});
							});
						});	
					},
					buildChapterSponsorListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								if (that.slugify(_.unescape(value_chaptersData.the_title)) == postID ){
									thisPostId = value_chaptersData.post_id
								}
							});
							that.returnAndSaveJsonData('listSponsors', function(sponsorsData){
								_.each(sponsorsData, function (value_sponsorsData, index_sponsorsData) {
									if(value_sponsorsData.sponsor_chapter  == thisPostId){
									returnObject = that.renderModel(value_sponsorsData, $(that.php_sponsor_item));
									if(value_sponsorsData.sponsor_url  != ''){
										returnObject.find('.sponsorLogo').wrap('<a href=' + value_sponsorsData.sponsor_url + ' target="_blank"></a>');
									} else {
										returnObject.find('.sponsorLogo').wrap('<div class="sponsorItemWrapper"></div>');
									}
									target.append(returnObject)
									if(value_sponsorsData.featuredImage != null){
										returnObject.find('.sponsorLogo').backstretch(value_sponsorsData.featuredImage)
									}
									}
								})	
							});
						});
					},
					buildChapterColorsListing: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							_.each(chaptersData, function (value_chaptersData, index_chaptersData) {
								returnObject = that.renderModel(value_chaptersData, $(that.php_chapter_colors_item));
								returnObject.find('.chapterColorBox').css('background-color', value_chaptersData.chapter_color)
								target.append(returnObject);
							})	
						})		
					},
					renderEventsCalendar: function(target){
						var that = this;	
						that.returnAndSaveJsonData('listChapters', function(chaptersData){
							that.returnAndSaveJsonData('listEvents', function(eventsData) {
								eventsArray = [];
								_.each(eventsData, function(event_value, event_key) {
									eventItem = {};
									eventTitle = event_value.the_title;
									eventTitleClean = (eventTitle.replace(';#8217;', '').replace(';#8211;', '').replace('&amp', '').replace('&amp;#8211;', '-'));
									eventItem['the_url'] = that.slugify(event_value.the_title + "-" + event_value.event_start_date);
									eventItem['title'] = _.unescape(eventTitleClean);
									eventItem['start'] = moment(event_value.event_start_date, "X").tz("America/New_York").format("YYYY-MM-DDTHH:mm:ss");
									eventItem['end'] = moment(event_value.event_end_date, "X").tz("America/New_York").format("YYYY-MM-DDTHH:mm:ss");
									_.each(chaptersData, function(value_chaptersData, index_chaptersData) {
										if ( parseInt(event_value.event_chapter) == value_chaptersData.post_id) {
											eventItem['event_color'] = value_chaptersData.chapter_color;
										}
									});
									eventsArray.push(eventItem);
								});
								target.fullCalendar({
									events: eventsArray,
									header: {
										left: '',
										center: 'prev title next',
										right: '' 
									},
									dayNamesShort: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
									eventAfterRender: function(event, element, view) {
										$('.fc-content').find(".fc-time").remove();
										if(event.event_color != ""){
											element.css('color', event.event_color)
											element.css('border-bottom', 'solid 5px' + event.event_color);
										}
									},
									eventClick: function(calEvent, jsEvent, view) {
										that.pushPageNav('events', that.slugify(calEvent.the_url));
									}
								});
							});
						});
					},
					returnFormattedTimeRange: function(startTime, endTime) {
						var that = this;	
						returnTime = "";
						if(endTime < startTime){
							returnTime = moment(startTime, "X").tz("America/New_York").format("MMMM Do YYYY, h:mm a");
						} else {
							startMonthDayYear = moment(startTime, "X").tz("America/New_York").format("MMMM Do YYYY");
							endMonthDayYear = moment(endTime, "X").tz("America/New_York").format("MMMM Do YYYY");
							startHourMinute = moment(startTime, "X").tz("America/New_York").format("h:mm a");
							endHourMinute = moment(endTime, "X").tz("America/New_York").format("h:mm a");
							if(startMonthDayYear == endMonthDayYear){
								if(startHourMinute == endHourMinute){
									returnTime = moment(startTime, "X").tz("America/New_York").format("dddd, MMMM Do YYYY") + ", " + moment(startTime, "X").tz("America/New_York").format("h:mm a");
								} else {
									returnTime = moment(startTime, "X").tz("America/New_York").format("dddd, MMMM Do YYYY, h:mm a") + " - " + moment(endTime, "X").tz("America/New_York").format("h:mm a");
								}
							} else {
								returnTime = moment(startTime, "X").tz("America/New_York").format("dddd, MMMM Do YYYY, h:mm:ss a") + " - " + moment(endTime, "X").tz("America/New_York").format("dddd, MMMM Do YYYY, h:mm:ss a");
							}
						}
						return returnTime;
					},
					buildSubNav: function(target){
						var that = this;
						checkPageNames = (postID == "") ? pageID : postID;
						if (pageID == "national-board") {
							checkPageNames = "national-board"
							}
						$('.menu-item').find('a').each(function(){ 
							pageNames = that.slugify($(this).text());
							if (pageNames == checkPageNames) {
								subMenu = "";
								if ($(this).parents('ul').hasClass('sub-menu')) {
									subMenu = $(this).parent().parent().clone();
								} else {
									if($(this).siblings('ul').size() == 1){
										subMenu = $(this).siblings('ul').clone();
									}
								}
								if (subMenu !== "") {
									subMenu.removeClass('dropdown-menu');		
								}
								target.html(subMenu);	
							}
						})
						$('.subNavContainer').find('a').off('click');
						$('.subNavContainer').find('a').on('click', function(e){
							e.preventDefault();	
							if((!$(this).parent().hasClass('hyperlink')) && (!$(this).hasClass('hyperlink'))){
								pageIDrequest = $(this).data('pageid');
								postIDrequest = $(this).data('postid');
								that.pushPageNav(pageIDrequest, postIDrequest);
							} else {
								window.open($(this).attr('href'), '_blank');
							}		
						})
					},
					bindMenuAnimations: function(menuTarget){
						menuTarget.children('li').mouseenter(function(){
							var hoverElement = $(this);
							var animationElement = hoverElement.children('ul');

							if(animationElement.size() > 0){

								dynamics.css(animationElement[0], {
									opacity: 0,
									display: 'block',
									scale: 1.1
								})
								dynamics.animate(animationElement[0], {
									scale: 1,
									opacity: 1
								}, {
									change: function(){
										if(typeof hoverElement.firstRun === "undefined" || hoverElement.firstRun == 0){
											hoverElement.firstRun = 1;
											hoverElement.siblings('li').children('ul').css('display','none');

											hoverElement.children('ul').children('li').each(function(index){
												var item = $(this)[0]
												dynamics.css(item, {
													opacity: 0,
													translateY: 20
												})

												dynamics.animate(item, {
													opacity: 1,
													translateY: 0
												}, {
													type: dynamics.spring,
													frequency: 300,
													friction: 435,
													duration: 1000,
													delay: 100 + index * 40
												})
											})

										}
									},
									complete: function(){
									},
									duration: 1000,
									type: dynamics.spring,
									frequency: 300,
									friction: 900,
								})
							}
						}).mouseleave(function(){
							var hoverElement = $(this);
							var animationElement = hoverElement.children('ul');						

							if(animationElement.size() > 0){
								dynamics.animate(animationElement[0], {
									opacity: 0
								}, {
									complete: function(){
										animationElement.css('display', 'none');
									},
									delay: 300,
									duration: 300
								})
							}
						});					
					},
					createAccessor: function(dataSource, variableName){
						var that = this;
						if(typeof that['accessor_' + variableName] === "undefined"){
							that['accessor_' + variableName] = {};
							if(variableName == 'listPages' || variableName == 'listChapters'){
								that['accessor_id_' + variableName] = {};
								_.each(dataSource, function(valuePagesData, indexPagesData){
									that['accessor_' + variableName][that.slugify(valuePagesData.the_title)] = valuePagesData
									that['accessor_id_' + variableName][valuePagesData.post_id] = valuePagesData
								});
							} else {
								_.each(dataSource, function(valuePagesData, indexPagesData){
									that['accessor_' + variableName][valuePagesData.post_id] = valuePagesData
								});
							}
						}
					},
					fixLinks: function(params){
						var that = this;
						$('.navbar, .footer').find('a').each(function(){
							var isInternalLink = (($(this).parent().hasClass('hyperlink')) || ($(this).hasClass('hyperlink'))) ? false : true;
							var isNotEmail =  ($(this).attr('href').indexOf('mailto:') == -1) ? true : false;
							var isLogo = ($(this).hasClass('navbar-brand')) ? true : false;

							if(isInternalLink && isNotEmail){
								var pageSlug = (isLogo) ? that.defaultPage : that.slugify($(this).text().replace('&amp;', ''))
								var newURL = '/' + pageSlug + '/';
								$(this).attr('href', newURL);
								$(this).addClass('menu-item-' + pageSlug);
								$(this).attr('data-pageid', pageSlug);

								$(this).off('click');
								$(this).on('click', function(e){
									e.preventDefault();
									that.pushPageNav($(this).attr('data-pageid'))
								})
							}
						});
					},
					chapterTypesMenu: function(params){
						var that = params.context;
						var dataSource = params.dataSource;

						$('#menu-main-menu').find('a').each(function(){
							if((that.slugify(_.unescape($(this).text()))) == params.chaptersMenuToRemove) {
								chaptersMenuToRemove = $(this).parent().detach();
							}
							if((webApp.slugify(_.unescape($(this).text()))) == params.chaptersMenuTarget) {
								chaptersMenuTarget = $(this).parent().find('ul')
							}
						});	

						var buildChaptersMenuDOM = function(){
							_.each(dataSource, function(value_chapterObjects, index_chapterObjects){
								chaptersMenuObject = webApp.renderModel(value_chapterObjects, $(webApp.php_chapter_subnav_item));
								chaptersMenuObject.find('a').attr('href', '/chapters/' + webApp.slugify(_.unescape(value_chapterObjects.the_title)) + '/');
								chaptersMenuObject.find('a').attr('data-pageid', 'chapters');
								chaptersMenuObject.find('a').attr('data-postid', webApp.slugify(value_chapterObjects.the_title));
								chaptersMenuObject.find('a').data('pageid', 'chapters');
								chaptersMenuObject.find('a').data('postid', webApp.slugify(_.unescape(value_chapterObjects.the_title)));
								chaptersMenuObject.find('a').html(_.unescape(value_chapterObjects.the_title));
								chaptersMenuTarget.append(chaptersMenuObject);
							});
							chaptersMenuTarget.find('a').off('click.chaptersMenu')
							chaptersMenuTarget.find('a').on('click.chaptersMenu', function(e){
								e.preventDefault();
								pageIDrequest = $(this).data('pageid');
								postIDrequest = $(this).data('postid');
								webApp.pushPageNav(pageIDrequest, postIDrequest);
							})
						};		

						return {
							init: function(){
								buildChaptersMenuDOM();
							}
						}
					},
					cleanUp: function(pageIDrequest, postIDrequest){
						var that = this;

						// if IE 8
						if(!($('html').hasClass('internet-explorer-8') || js_mobile == "true")){
							that.bindMenuAnimations($('#menu-main-menu'));
						}

						if ($(".chapterTypes").length == 0){
							that.returnAndSaveJsonData('listChapters', function(chaptersData) {
								chaptersMenu = new that.chapterTypesMenu({
									context: that,
									dataSource: chaptersData,
									chaptersMenuTarget: 'chapters',
									chaptersMenuToRemove: 'chapters-sub-menu',
								});
								chaptersMenu.init()
								if(js_mobile == "true"){
									if($('#mm-navbar').size() == 0){
										that.multiLevelPushMenu(jQuery('#navbar'))
									}
									$("#mm-navbar").trigger("close.mm");
								}
							});
						}

						if($('.loginFormWrapper').html() == ''){
							$('.loginFormWrapper').html(that.php_login_form);
						}

						// page specific cleanups
						switch(pageIDrequest){
							case 'home':
							$('.slideShowContainer').remove();
							$('.subNavContainer').remove();
							$('.bannerMap').remove();
							break;

							case 'events':
							$('.subNavContainer').remove();
							$('.bannerMap').remove();
							break;

							case 'gallery':
							$('.subNavContainer').remove();
							$('.bannerMap').remove();
							$('.slideShowContainer').remove();
							break;

							case 'contact':
							$('.slideShowContainer').remove();
							$('.subNavContainer').remove();
							break;

							default:
							$('.bannerMap').remove();
							break;
						}
					},
					adminPages: function(currentPage){
						var that = this;
						switch(currentPage){
							case "galleries":
							that.renderGalleriesAdmin();
							break;

							case "page":
							case "events":
							that.featuredImagesLauncher();
							break;
						}
					},
					singlePageAnimate: function(pageIDrequest){
						var that = this;
						that.animateSomething({
							target: $('body'),
							animation: {
								scrollTop: $('#page_' + pageIDrequest).offset().top - $('.pageSection').offset().top,
							},
							time: 400,
							callback: function() {
								console.log('page loaded')
							}
						})
					},
					setPopstate: function(){
						var that = this;
						if( that.isSinglePage() ){

							$(window).on('popstate',function(){
								if(that.startUpRan){
									postID = "";
									previousPage = pageID;


									if(history.state != null){
										that.setPageID(that.RewriteBase, history.state.pageID);
										that.singlePageAnimate(pageID)

									} else {
										loadPage = that.cleanPageState(window.location.pathname.replace(that.RewriteBase, ""));

										postIDtest = window.location.pathname.split("/");
										if(postIDtest[postIDtest.length-1] == ""){
											postIDtest.pop();
										}
										if($.isNumeric(postIDtest[postIDtest.length-1])){
											postID = postIDtest[postIDtest.length-1];
										}

										if(loadPage == ""){
											loadPage = that.defaultPage;
										}
										pageID = loadPage;

										postIDpath = (history.state == null) ? "" : history.state.pageID;
										that.setPageID(that.RewriteBase, postIDpath);
										that.singlePageAnimate(pageID)

									}
								}
							});


						} else {
							$(window).on('popstate',function(){
								if(that.startUpRan){
									postID = "";
									previousPage = pageID;


									if(history.state != null){
										that.animateSomething({
											target: $('.pageSection'),
											animation: {
												opacity: 0,
											},
											time: 200,
											callback: function(){
												that.setPageID(that.RewriteBase, history.state.pageID);
												that.gaSendPageView();
												that.loadView(pageID, postID);
											}
										})
									} else {
										loadPage = that.cleanPageState(window.location.pathname.replace(that.RewriteBase, ""));

										postIDtest = window.location.pathname.split("/");
										if(postIDtest[postIDtest.length-1] == ""){
											postIDtest.pop();
										}
										if($.isNumeric(postIDtest[postIDtest.length-1])){
											postID = postIDtest[postIDtest.length-1];
										}

										if(loadPage == ""){
											loadPage = that.defaultPage;
										}
										pageID = loadPage;

										that.animateSomething({
											target: $('.pageSection'),
											animation: {
												opacity: 0,
											},
											time: 200,
											callback: function(){
												var pastState;
												if(history.state != null){
													pastState = history.state.pageID;
												} else {
													pastState = "";
												}
												that.setPageID(that.RewriteBase, pastState);
												that.gaSendPageView();
												that.loadView(pageID, postID);
											}
										})

									}
								}
							});
						}
					},
					pushPageNav: function(pageIDrequest, postIDrequest){
						var that = this;
						if(typeof postIDrequest === "undefined"){
							postIDrequest = "";
							postIDurl = "";
						} else {
							postIDurl = postIDrequest + "/";
						}



						if( that.isSinglePage() ){

							if((window.location.pathname.charAt(window.location.pathname.length-1) == "/") && (window.location.pathname != that.RewriteBase)){
								newPage = '/' + pageIDrequest + "/" + postIDurl;
							} else {
								newPage = '/' + pageIDrequest + "/" + postIDurl;
							}
							var stateObj = { pageID: newPage};
							if (Modernizr.history){
								history.pushState(stateObj, null, newPage);
							}
							that.setPageID(that.RewriteBase)

							that.singlePageAnimate(pageIDrequest)

						} else {
							if (Modernizr.history){
								that.animateSomething({
									target: $('.pageSection'),
									animation: {
										opacity: 0,
									},
									time: 200,
									callback: function() {
										if((window.location.pathname.charAt(window.location.pathname.length-1) == "/") && (window.location.pathname != that.RewriteBase)){
											newPage = '/' + pageIDrequest + "/" + postIDurl;
										} else {
											newPage = '/' + pageIDrequest + "/" + postIDurl;
										}
										var stateObj = { pageID: newPage};
										history.pushState(stateObj, null, newPage);
										that.setPageID(that.RewriteBase)
										that.gaSendPageView();
										that.loadView(pageID, postID);
									}
								})
							} else {
								window.location = that.pathPrefix + pageIDrequest + "/" + postIDurl;
							}
						}

					},
					multiPageSaveAjax: function(){
						var that = this;
						var recursiveSEO  = function () {
							var page_array_index = 0;
							var time_to_wait_on_page = 3000;



							var initialize = function (this_list_of_pages) {
								window['list_of_pages'] = this_list_of_pages;
								recursive_page_change()
							}

							var recursive_page_change = function(){
								// log the current number until it reaches our logic constraint
									var this_page_item = window['list_of_pages'][page_array_index];
									var newPostID = (this_page_item.postID != null) ? this_page_item.postID : "";
									that.pushPageNav(this_page_item.pageID, newPostID);

								// increment the default number
									page_array_index++;


								setTimeout(function(){
									// check if the test number is less than 100 AND less then our array length
										console.log(pageID, postID)
										that.saveAjax();
										if(page_array_index < window['list_of_pages'].length){
											recursive_page_change();
										}
								}, time_to_wait_on_page);
										
									
							}

							var debug0 = function(value){
								console.log(value);
								$('#container').html(value);
							}

							var generate_xml = function (this_list_of_pages) {
								var returnObject = $('<urlset />');
								returnObject.attr({
									'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
									'xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1'
								})

								_.each(this_list_of_pages, function (value_this_list_of_pages) {
									var url = $('<url />');
									var loc = $('<loc />');
									var changefreq = $('<changefreq />');

									var this_pageid = value_this_list_of_pages.pageID;
									var this_postid = value_this_list_of_pages.postID;

							    	var url_string = (typeof this_postid === "undefined" || this_postid == null || this_postid == "") ? webApp.pathPrefix + this_pageid + '/' : webApp.pathPrefix + this_pageid + '/' + this_postid + '/';


									loc.html(url_string);
									changefreq.html('weekly');

									url.append(loc)
									url.append(changefreq)

									returnObject.append(url)
								})

								console.log(returnObject[0])
							}

							return {
								initialize: initialize,
								generate_xml: generate_xml
							}
						}

							that.returnAndSaveJsonData('listEvents', function (eventsData) {
								that.returnAndSaveJsonData('listChapters', function (chaptersData) {
									that.returnAndSaveJsonData('listPages', function (pagesData) {
										var pagesWithPostIds = ['chapters', 'events', 'gallery'];
										var finalArray = [];
										_.each(pagesData, function (value_pagesData, index_pagesData) {
											var thisPageSlug = that.slugify(_.unescape(value_pagesData.the_title));

											if($.inArray(thisPageSlug, pagesWithPostIds) == -1){
												// if this page does not have post IDs
													var thisObject = {};
													thisObject['pageID'] = thisPageSlug;
													thisObject['postID'] = null;
													finalArray.push(thisObject);
											} else {
												var thisObject = {};
												thisObject['pageID'] = thisPageSlug;
												thisObject['postID'] = null;
												finalArray.push(thisObject);

												switch(thisPageSlug){

													case 'chapters':
														_.each(chaptersData, function(value, inde){
															var this_post_id_slug = that.slugify(_.unescape(value.the_title))
															var thisObject = {};
															thisObject['pageID'] = thisPageSlug;
															thisObject['postID'] = this_post_id_slug;
															finalArray.push(thisObject);
														})

													break;

													case 'events':
														_.each(eventsData, function(value, inde){
															var this_post_id_slug = that.slugify(_.unescape(value.the_title) + " " + value.event_start_date)
															var thisObject = {};
															thisObject['pageID'] = thisPageSlug;
															thisObject['postID'] = this_post_id_slug;
															finalArray.push(thisObject);
														})

													break;

													case 'gallery':
														_.each(eventsData, function(value, inde){
															var this_post_id_slug = that.slugify(_.unescape(value.the_title))
															var thisObject = {};
															thisObject['pageID'] = thisPageSlug;
															thisObject['postID'] = this_post_id_slug;
															finalArray.push(thisObject);
														})

													break;


												}

											}
										});
										// _.each(finalArray, function (value_finalArray) {
										// 	console.log(value_finalArray)
										// })
										var this_seo_instance = new recursiveSEO();
										this_seo_instance.initialize(finalArray)
										// this_seo_instance.generate_xml(finalArray)

									})
								});
							});
					}
				
				}
			});
		}

	});

})(jQuery);