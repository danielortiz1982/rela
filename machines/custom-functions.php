<?php
	// define('WP_HOME','http://example.com');
	// define('WP_SITEURL','http://example.com');

// WEB CRAWLER
function my_page_template_redirect(){
		// default pageID
		$defaultPageID = "home";

		// site base (if none simply use "/")
		$siteBase = "";

		// $acceesFile = file_get_contents('http://bermangrp.com/register/access.json');
		// $validIP = json_decode($acceesFile);

//		$validIP = array('38.98.105.2');

		$ipOverride = false;

		// if(in_array($_SERVER['REMOTE_ADDR'], $validIP) || ($ipOverride === true) || ($_SERVER['QUERY_STRING'] == "preview") || (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),'google') !== false)){
			$isEscapeFragment = (strpos($_SERVER["REQUEST_URI"],'_escaped_fragment_') !== false);
			$isBing = (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),'bing') !== false);
			$isGoogle = (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),'google') !== false);
			if ($isEscapeFragment || $isBing || $isGoogle) {
				$ajaxPageID = "";
				$pageURL = str_replace($siteBase, "", $_SERVER["REQUEST_URI"]);
				$pageURL = str_replace("?_escaped_fragment_=", "", $pageURL);
				$pageURL = str_replace("?_escaped_fragment_", "", $pageURL);
				$pageURLarray = explode("/", $pageURL);

				$newString = array();
				foreach ($pageURLarray as $key => $value) {
					if($value != ""){
						$newString[] = $value;
					}
				}
				$file_name = implode("-", $newString);
				$file_name = ($file_name == "") ? $defaultPageID : $file_name;

			    include(TEMPDIR . '/_ajax/' . $file_name . '.html');
				exit();

				// switch ($pageURL) {
				// 	case "":
				// 		$ajaxPageID = $defaultPageID;
				// 	break;

				// 	default:
				// 		$pageURLarray = explode("/", $pageURL);
				// 		$ajaxPageID = $pageURLarray[0];
				// 	break;
					
				// }
				// //echo TEMPDIR . '/_ajax/' . $ajaxPageID . '.html';

			 //    include(TEMPDIR . '/_ajax/' . $ajaxPageID . '.html');
				// exit();
			}
		// } else {
		// 	header("Location: http://www.statenislandbeachfest.com/home.html");

		// 	exit();
		// }
	}
	add_action( 'template_redirect', 'my_page_template_redirect', 0);

	// REMOVE EMOJIES
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// REMOVE HEAD META TAGS
	remove_action('wp_head', 'rel_canonical');
	remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
	remove_action('wp_head', 'wp_generator'); // remove wordpress version

	remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
	remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

	remove_action('wp_head', 'index_rel_link'); // remove link to index page
	remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

	remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
	remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

	// LOAD SCRIPTS
    add_action( 'admin_init', 'loadScripts' );
    add_action( 'wp_enqueue_scripts', 'loadScripts' );
    function loadScripts() {
    	wp_register_script('underscore-local', PAGEDIR . '/machines/libraries/underscore/underscore-min.js');
    	wp_enqueue_script('underscore-local');

		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_script('jquery-ui-sortable');
		// wp_enqueue_script('jquery-ui-resizable');
		wp_enqueue_script('jquery-ui-draggable');
		wp_enqueue_script('jquery-ui-droppable');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

    	wp_register_script('timepicker', PAGEDIR . '/machines/libraries/timepicker/timepicker.js', array('jquery-ui-datepicker'), '1.0', true );
    	wp_enqueue_script('timepicker');

    	// wp_register_script('easyListSplitter', PAGEDIR . '/machines/libraries/easyListSplitter/easyListSplitter.js', array('jquery-ui-datepicker'), '1.0', true );
    	// wp_enqueue_script('easyListSplitter');

    	wp_register_script('moment', PAGEDIR . '/machines/libraries/moment/moment.min.js', array('jquery-ui-datepicker'), '1.0', true );
    	wp_enqueue_script('moment');

		wp_enqueue_style('spectrum-style', PAGEDIR . '/machines/libraries/spectrum/spectrum.css');
    	wp_register_script('spectrum', PAGEDIR . '/machines/libraries/spectrum/spectrum.js', array('jquery'), '1.0', true );
    	wp_enqueue_script('spectrum');

		wp_enqueue_style('shadowbox-style', PAGEDIR . '/machines/libraries/shadowbox/shadowbox.css');
    	wp_register_script('shadowbox', PAGEDIR . '/machines/libraries/shadowbox/shadowbox.js', array('jquery'), '1.0', true );
    	wp_enqueue_script('shadowbox');

    	wp_enqueue_style('focuspoint-style', PAGEDIR . '/machines/libraries/focuspoint/focuspoint.css');
    	wp_register_script('focuspoint', PAGEDIR . '/machines/libraries/focuspoint/jquery.focuspoint.min.js', array('jquery'), '1.0', true );
    	wp_enqueue_script('focuspoint');

    	wp_register_script('boilerplate', PAGEDIR . '/machines/libraries/boilerplate/boilerplate.js', array('jquery'), '1.0', true );
    	wp_enqueue_script('boilerplate');

    	wp_register_script('dynamics', PAGEDIR . '/machines/dynamics.js', array('underscore-local', 'timepicker', 'boilerplate'), '1.0', true );
    	wp_enqueue_script('dynamics');

    }

// CUSTOM PAGE 
	include(TEMPDIR . '/machines/functions/custom_page.php');

// CUSTOM POST TYPE HOME IMAGES
	include(TEMPDIR . '/machines/functions/custom_post_galleries.php');

	$modelJSON = file_get_contents(TEMPDIR . '/machines/functions/models.json');
	$models = json_decode($modelJSON, true);
	$modelLogic = buildModels($models);
	eval($modelLogic);

// BUILD MODELS
	function buildModels($modelRequest){
		$post_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/custom_post_template.php');
		$inputs_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/inputs.php');
		$input_text_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_text.php');
		$input_date_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_date.php');
		$input_colorpicker_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_colorpicker.php');
		$input_editor_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_editor.php');
		$input_checkbox_multi_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_checkbox_multi.php');
		$input_radio_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_radio.php');
		$input_select_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_select.php');
		$input_checkbox_single_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_checkbox_single.php');
		$input_focuspoint_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_focuspoint.php');
		$input_hidden_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_hidden.php');
		$input_featured_images_template = file_get_contents(TEMPDIR . '/machines/functions/post_template/input_featured_images.php');

		$evalText = "";
		foreach ($modelRequest as $key_models => $value_models) {
			$thisFile = $post_template;
			$modelPlural = $value_models['plural'];
			$modelSingular = $value_models['singular'];
			$modelSupports = $value_models['supports'];
			$modelPluralSlug = str_replace(' ', '', $value_models['plural']);
			$modelSlug = strtolower(str_replace(' ', '_', $value_models['plural']));

			if(count($value_models['fields']) == 0){
				$thisFile = str_replace('_modelFields_', "", $thisFile);
			} else {
				$fieldText = "";
				foreach ($value_models['fields'] as $key_this_field => $value_this_field) {

					switch ($value_this_field['type']) {
						case 'input_text':
							$this_input_text = $input_text_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_text = str_replace('_fieldSlug_', $this_field_slug, $this_input_text);
							$this_input_text = str_replace('_fieldProper_', $this_field_proper, $this_input_text);

							$fieldText .= $this_input_text;
						break;

						case 'input_date':
							$this_input_date = $input_date_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_date = str_replace('_fieldSlug_', $this_field_slug, $this_input_date);
							$this_input_date = str_replace('_fieldProper_', $this_field_proper, $this_input_date);

							$fieldText .= $this_input_date;
						break;

						case 'input_colorpicker':
							$this_input_colorpicker = $input_colorpicker_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];
							$thisFieldColors = $value_this_field['field_colors'];

							$this_input_colorpicker = str_replace('_fieldSlug_', $this_field_slug, $this_input_colorpicker);
							$this_input_colorpicker = str_replace('_fieldProper_', $this_field_proper, $this_input_colorpicker);
							$this_input_colorpicker = str_replace('_fieldColors_', $thisFieldColors, $this_input_colorpicker);

							$fieldText .= $this_input_colorpicker;
						break;

						case 'input_editor':
							$this_input_editor = $input_editor_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_editor = str_replace('_fieldSlug_', $this_field_slug, $this_input_editor);
							$this_input_editor = str_replace('_fieldProper_', $this_field_proper, $this_input_editor);

							$fieldText .= $this_input_editor;
						break;

						case 'input_checkbox_multi':
							$this_input_checkbox_multi = $input_checkbox_multi_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];
							$this_field_feed = $value_this_field['field_feed'];

							$this_input_checkbox_multi = str_replace('_fieldSlug_', $this_field_slug, $this_input_checkbox_multi);
							$this_input_checkbox_multi = str_replace('_fieldProper_', $this_field_proper, $this_input_checkbox_multi);
							$this_input_checkbox_multi = str_replace('_fieldFeed_', $this_field_feed, $this_input_checkbox_multi);

							$fieldText .= $this_input_checkbox_multi;
						break;

						case 'input_radio':
							$this_input_radio = $input_radio_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];
							$this_field_feed = $value_this_field['field_feed'];

							$this_input_radio = str_replace('_fieldSlug_', $this_field_slug, $this_input_radio);
							$this_input_radio = str_replace('_fieldProper_', $this_field_proper, $this_input_radio);
							$this_input_radio = str_replace('_fieldFeed_', $this_field_feed, $this_input_radio);

							$fieldText .= $this_input_radio;
						break;

						case 'input_select':
							$this_input_select = $input_select_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];
							$this_field_feed = $value_this_field['field_feed'];

							$this_input_select = str_replace('_fieldSlug_', $this_field_slug, $this_input_select);
							$this_input_select = str_replace('_fieldProper_', $this_field_proper, $this_input_select);
							$this_input_select = str_replace('_fieldFeed_', $this_field_feed, $this_input_select);

							$fieldText .= $this_input_select;
						break;

						case 'input_checkbox_single':
							$this_input_checkbox_single = $input_checkbox_single_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_checkbox_single = str_replace('_fieldSlug_', $this_field_slug, $this_input_checkbox_single);
							$this_input_checkbox_single = str_replace('_fieldProper_', $this_field_proper, $this_input_checkbox_single);

							$fieldText .= $this_input_checkbox_single;
						break;

						case 'input_focuspoint':
							$this_input_focuspoint = $input_focuspoint_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_focuspoint = str_replace('_fieldSlug_', $this_field_slug, $this_input_focuspoint);
							$this_input_focuspoint = str_replace('_fieldProper_', $this_field_proper, $this_input_focuspoint);

							$fieldText .= $this_input_focuspoint;
						break;

						case 'input_hidden':
							$this_input_hidden = $input_hidden_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_hidden = str_replace('_fieldSlug_', $this_field_slug, $this_input_hidden);
							$this_input_hidden = str_replace('_fieldProper_', $this_field_proper, $this_input_hidden);

							$fieldText .= $this_input_hidden;
						break;

						case 'input_featured_images':
							$this_input_featured_images = $input_featured_images_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_featured_images = str_replace('_fieldSlug_', $this_field_slug, $this_input_featured_images);
							$this_input_featured_images = str_replace('_fieldProper_', $this_field_proper, $this_input_featured_images);

							$fieldText .= $this_input_featured_images;
						break;

						case 'input_wp_featured_images':
							$this_input_wp_featured_images = $input_wp_featured_images_template;
							$this_field_slug = $value_this_field['field_slug'];
							$this_field_proper = $value_this_field['field_proper'];

							$this_input_wp_featured_images = str_replace('_fieldSlug_', $this_field_slug, $this_input_wp_featured_images);
							$this_input_wp_featured_images = str_replace('_fieldProper_', $this_field_proper, $this_input_wp_featured_images);

							$fieldText .= $this_input_wp_featured_images;
						break;
					}

				}

				$thisFile = str_replace('_modelFields_', $fieldText, $thisFile);

			}

			$thisFile = str_replace('<?php', '', $thisFile);
			$thisFile = str_replace('?>', '', $thisFile);

			$thisFile = str_replace('_modelPlural_', $modelPlural, $thisFile);
			$thisFile = str_replace('_modelSingular_', $modelSingular, $thisFile);
			$thisFile = str_replace('_modelSlug_', $modelSlug, $thisFile);
			$thisFile = str_replace('_modelPluralSlug_', $modelPluralSlug, $thisFile);
			$thisFile = str_replace('_supportArray_', $modelSupports, $thisFile);


			$evalText .= $thisFile;
		}

		return $evalText;

	}

	
// GENERATE META BOXES
	function generateMetaBoxes($arrayData){
		foreach ($arrayData as $value) {
			add_meta_box($value['id'], $value['name'], "populateMetaBoxes", $value['post_type'], $value['position'], $value['priority'], $value['callback_args']);
		}
	}

// POPULATE META BOXES
	function populateMetaBoxes($post, $arguments){
		$argumentData = $arguments['args'];
		switch ($argumentData['input_type']) {
			case 'input_text':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				include(TEMPDIR . '/views/input_text.php');
			break;

			case 'input_editor':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];

				wp_editor( $inputValue, $inputID );

				include(TEMPDIR . '/views/js_disable_meta_box_sortable.php');
			break;

			case 'input_checkbox_single':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				$inputText = $argumentData['input_text'];
				include(TEMPDIR . '/views/input_checkbox_single.php');
			break;

			case 'input_checkbox_multi':
				$custom = get_post_custom($post->ID);
				$inputValue = unserialize($custom[$argumentData['input_name']][0]);
				$inputID = $argumentData['input_name'];
				$inputText = $argumentData['input_text'];
				$field_options = call_user_func($argumentData['input_source'], 'inputs');
				include(TEMPDIR . '/views/input_checkbox_multi.php');
			break;

			case 'input_radio':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				$inputText = $argumentData['input_text'];
				$field_options = call_user_func($argumentData['input_source'], 'inputs');
				include(TEMPDIR . '/views/input_radio.php');
			break;

			case 'input_select':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				$inputText = $argumentData['input_text'];
				$field_options = call_user_func($argumentData['input_source'], 'inputs');
				include(TEMPDIR . '/views/input_select.php');
			break;

			case 'input_date':
				$custom = get_post_custom($post->ID);
				$date = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				if($date != ""){
					$dateTimeReturn = new DateTime("@$date");
					$otherTZ  = new DateTimeZone('America/New_York');
					$dateTimeReturn->setTimezone($otherTZ);
					$dateTimeString = $dateTimeReturn->format('m/d/Y H:i');
				}
				include(TEMPDIR . '/views/input_date.php');
			break;

			case 'input_hidden':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				include(TEMPDIR . '/views/input_hidden.php');
			break;

			case 'input_focuspoint':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				include(TEMPDIR . '/views/input_focuspoint.php');
			break;

			case 'input_featured_images':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				include(TEMPDIR . '/views/input_featured_images.php');
			break;

			case 'input_colorpicker':
				$custom = get_post_custom($post->ID);
				$inputValue = $custom[$argumentData['input_name']][0];
				$inputID = $argumentData['input_name'];
				$paletteSelection = "";
				if(isset($argumentData['input_palette'])){
					foreach ($argumentData['input_palette'] as $key => $value) {
						$paletteSelection .= "'" . $value . "', ";
					}
				}
				include(TEMPDIR . '/views/input_colorpicker.php');
			break;

		}
	}

// SAVE POST DATA
	function savePostData($arrayData){
		global $post;
		global $wpdb;
		if(isset($arrayData)){
			foreach ($arrayData as $value) {
				update_post_meta($post->ID, $value['callback_args']['input_name'], $_POST[$value['callback_args']['input_name']]);
			}
		}
		if($wpdb->get_row("SELECT meta_value FROM wp_postmeta WHERE post_id = $post->ID AND meta_key = 'custom_order'") == null){
			update_post_meta($post->ID, "custom_order", '-1');
		}
	}

// RETURN MODEL LIST
	function returnModelList($context){
		$excludeArray = array('post', 'page', 'attachment', 'revision', 'nav_menu_item', 'layouts');
		switch ($context) {
			case 'inputs':
				$post_types = get_post_types( '', 'names' ); 
				$field_options = array();
				foreach ( $post_types as $post_type ) {
					if(in_array($post_type, $excludeArray) === false){
						$checkBoxOption = array(
							"id" => $post_type,
							"name" => $post_type,
						);
						$field_options[] = $checkBoxOption;
					}
				}

				return $field_options;

			break;
		}
	}


// RETURN DATA
	function returnData($loopArgs, $metaBoxData, $dataType, $jsonVarName = 'json_data', $taxonomyData = null){
		global $post;

		$loop = new WP_Query($loopArgs);
		$returnDataArray = array();
		
		foreach ($loop->posts as $key => $postValue) {
			$attachmentArray = getAttachmentArray($postValue->ID);

			if (has_post_thumbnail( $postValue->ID ) ){
				$featuredImageArray = wp_get_attachment_image_src( get_post_thumbnail_id( $postValue->ID ), 'single-post-thumbnail' );
				$featuredImageURL = $featuredImageArray[0];
			} else {
				$featuredImageURL = null;
			}

			if($featuredImageURL == "" || $featuredImageURL == null){
				$featuredImageSizeArray = array();
			} else {
				$imageSizeArray = getimagesize($featuredImageURL);
				$featuredImageSizeArray = array(
					'width' => $imageSizeArray[0],
					'height' => $imageSizeArray[1],
				);
			}

			$array = array(
				"post_id" => $postValue->ID,
				"the_title" => htmlspecialchars(get_the_title($postValue->ID), ENT_QUOTES),
				"the_content" => htmlspecialchars(get_post_field('post_content', $postValue->ID), ENT_QUOTES),
				"attachments" => $attachmentArray,
				"featuredImage" => $featuredImageURL,
				"featuredImageSize" => $featuredImageSizeArray
			);

			if(!is_null($taxonomyData)){
				foreach ($taxonomyData as $value) {
					$array[$value] = getTaxonomyData($postValue->ID, $value);
				}
			}

			if(isset($metaBoxData)){
				foreach ($metaBoxData as $value) {
					$valueData = get_post_meta($postValue->ID, $value['callback_args']['input_name'], true);
					if(is_array($valueData)){
						$array[$value['callback_args']['input_name']] = $valueData;
					} else {
						$jsonTest = json_decode($valueData);
						if (($jsonTest != null) && (!is_numeric($jsonTest))) {
							$array[$value['callback_args']['input_name']] = json_decode($valueData);
						} else {
							$array[$value['callback_args']['input_name']] = htmlspecialchars($valueData, ENT_QUOTES);
						}
					}
				}
			}
			$returnDataArray[] = $array;

			if($loopArgs['post_type'] == "galleries"){
				$imageIDarray = get_post_meta($postValue->ID, 'galleries_gallery_image_array', true);
				$imageIDarray = explode(",", $imageIDarray);

				$returnArray = array();

				foreach ($imageIDarray as $value) {

					$returnArray[] = buildAttachmentData($value);
				}

				$array["galleries_json"] = $returnArray;
			}

		}
		
		switch ($dataType) {
			case 'json':
				populateJSON($returnDataArray, $jsonVarName);
			break;
			
			case 'array':
				return $returnDataArray;
			break;
			
			default:
				print_r($returnDataArray);
			break;
		}
	}

	function buildAttachmentData($attachmentID){
		$thumbNailArray = wp_get_attachment_image_src( $attachmentID , 'thumbnail' );
		$homeThumb = wp_get_attachment_image_src( $attachmentID , 'home-thumbs' );
		$fullSizeImage = wp_get_attachment_url( $attachmentID , false );
		$mediumArray = wp_get_attachment_image_src( $attachmentID , 'medium-portfolio' );
		$attachmentMeta = get_post( $attachmentID );
		$attachmentAltText = get_post_meta( $attachmentID, '_wp_attachment_image_alt', true );
		$attachmentData = array(
		    "full" => $fullSizeImage,
		    "thumb" => $thumbNailArray[0],
		    'medium' => $mediumArray[0],
		    'home_thumb' => $homeThumb[0],
		    "meta" => array(
		    	"title" => $attachmentMeta->post_title,
		    	"caption" => $attachmentMeta->post_excerpt,
		    	"alt_text" => $attachmentAltText,
		    	"description" => $attachmentMeta->post_content
		    	)
		);
		return $attachmentData;

	}

// POPULATE JAVASCRIPT VARIABLE WITH JSON DATA
	function populateJSON($jsonData, $variableName){
		include(TEMPDIR . '/views/js_json.php');
	}

// POPULATE JAVASCRIPT VARIABLE WITH JSON DATA
	function populateJavascript($javascriptData, $variableName){
		include(TEMPDIR . '/views/js_javascript.php');
	}

// GET ATTACHMENTS AS AN ARRAY
	function getAttachmentArray($postID){
		$args = array( 'post_type' => 'attachment', 'posts_per_page' => -1, 'post_status' =>'any', 'post_parent' => $postID ); 
		$attachments = get_posts( $args );
		if ( $attachments ) {
			$attachment_array = array();
			foreach ( $attachments as $attachment ) {
				$attachment_array[] = buildAttachmentData($attachment->ID);
			}
		}
		return $attachment_array;
	}

	function getFeatureImageArray($postID) {
		$featureThumb = wp_get_attachment_image_src( $postID, 'thumbnail');
		$featureMed = wp_get_attachment_image_src( $postID, 'medium');
		$featureFull = wp_get_attachment_image_src( $postID, 'full');

		$feature_images_array = array(
			"full" => $featureFull[0],
			"medium" => $featureMed[0],
			"thumb" => $featureThumb[0]
		);

		return $feature_images_array;
	}

// AJAX SORT
	add_action('wp_ajax_update_sort', 'updateSortAjax');

	function updateSortAjax() {
		global $wpdb;
		$sort_data = $_POST['sort_data'];
		foreach ($sort_data as $key => $value) {
			if( $wpdb->update('wp_postmeta',array('meta_value' => $value),array( 'post_id' => $key, 'meta_key' => 'custom_order' )) === FALSE){
				print_r($wpdb->show_errors());
			} else {
				echo $key . ";" . $value . "\n";
			}
		}
		die();
	}

// GET TAXONOMY DATA
	function getTaxonomyData($postID, $taxonomyName){
		$terms = get_the_terms($postID, $taxonomyName);
		$returnDataArray = array();
		if($terms != ""){
			foreach ($terms as $term) {
				$returnDataArray[] = htmlspecialchars($term->name, ENT_QUOTES);
			}
		}
		return $returnDataArray;
	}

// CUSTOMIZE TINY MCE
	add_filter('tiny_mce_before_init', 'myformatTinyMCE' );
	function myformatTinyMCE($in){
		$in['wpautop']=false;
		// SET PASTE AS TEXT AS DEFAULT
		$in['paste_as_text'] = true;
		return $in;
	}

// ADD MAIN MENU
	add_action( 'init', 'register_main_menu' );
	function register_main_menu() {
		register_nav_menus(
			array(
				'header-menu' => __( 'Header Menu' ),
				'extra-menu' => __( 'Extra Menu' )
				)
			);
	}

// GENEREATE PAGES JSON
	function generatePagesJSON($currentPageID){
		$allPageIDs = get_all_page_ids();
		$returnArray = array();
		foreach ($allPageIDs as $value) {
			if($currentPageID == $value){
				$current_page = true;
			} else {
				$current_page = false;
			}

			if (has_post_thumbnail( $value ) ){
				$featuredImageArray = wp_get_attachment_image_src( get_post_thumbnail_id( $value ), 'full' );
				$featuredImageURL = $featuredImageArray[0];
			} else {
				$featuredImageURL = null;
			}

			$itemArray = array(
				'wp_page_id' => $value,
				'pageID' => sanitize_title(get_the_title($value)),
				'current_page' => $current_page,
				'pageTitle' => get_the_title($value),
				'featuredImage' => $featuredImageURL
			);
			$returnArray[] = $itemArray;
		}
		populateJSON($returnArray, 'pages');
	}

// INCLUDE POST THUMBNAILS
	add_action( 'after_setup_theme', 'includePostThumbnails' );

	function includePostThumbnails(){
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 348, 277, false );
	}

// HIDE ADMIN BAR ON FRONT ENT
	add_filter( 'show_admin_bar' , 'my_function_admin_bar');
	function my_function_admin_bar(){
		return false;
	}

// LIST FLICKR SETS
	function listFlickrSets($requestType = ''){
		
		$apiKey = '9c76207c71bf63679f8c173f3f02a441';
		$userID = '65441853@N03';
		$url = 'https://api.flickr.com/services/rest/?method=flickr.photosets.getList&user_id=' . $userID . '&api_key=' . $apiKey . '&format=php_serial';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);
		$result=unserialize($result);

		$photosetArray = $result['photosets']['photoset'];

		$field_options = array();
		foreach ($photosetArray as $key => $value) {
			$checkBoxOption = array(
				"id" => $value['id'],
				"name" => $value['title']['_content'],
			);
			$field_options[] = $checkBoxOption;
		}

		return $field_options;

	}

// CUSTOM PAGE 
	include(TEMPDIR . '/machines/functions/custom_user.php');

	function loadFilesToVariables($fileArray){
		foreach ($fileArray as $fileKey => $fileValue) {
			ob_start();
			include(TEMPDIR . '/' . $fileValue);
			$fileData = ob_get_clean();
			$fileData = stripslashes(preg_replace('/^\s+|\n|\r|\s+$/m', '', $fileData));

			$fileType = end(explode(".", $fileValue));

			populateJavascript($fileData, $fileType . "_" . $fileKey);
		}
	}

	// RETURN BROWSER NAME + VERSION
	require(TEMPDIR . '/machines/libraries/phpbrowser/Browser.php');
	function returnBrowser() {
		$browser = new Browser();

		$browserName = slugify($browser->getBrowser());
		$browserVersion = $browser->getVersion();
		$browserVersion = explode('.', $browserVersion);
		$browserVersion = $browserVersion[0];

		$browserClass = $browserName . '-' . $browserVersion;

		return $browserClass;
	}

	// Slugify string of text
	function slugify($text){
		$text = strtolower($text);
		$text = preg_replace('/\+/', '', $text); // Replace spaces with 
		$text = preg_replace('/\s+/', '-', $text); // Replace spaces with -
		$text = preg_replace('/[^\w\-]+/', '', $text); // Remove all non-word chars
		$text = preg_replace('/\-\-+/', '-', $text); // Replace multiple - with single -
		$text = preg_replace('/^-+/', '', $text); // Trim - from start of text
		$text = preg_replace('/-+$/', '', $text); // Trim - from end of text
		return $text;
	}

	// Adds additional file formats to the allowed upload extension list
	add_filter('upload_mimes', 'additionalFileTypes');
	function additionalFileTypes ( $existing_mimes=array() ) {
		$existing_mimes['vcf'] = 'text/x-vcard';
		return $existing_mimes;
	}

	// RETURN IF MOBILE
	require(TEMPDIR . '/machines/libraries/phpmobile/phpmobile.php');
	function returnMobile() {
		$mobile = 'false';
		$detect = new Mobile_Detect();

		if ( $detect->isMobile() ) {
			$mobile = 'true';
		}

		return $mobile;

	}

