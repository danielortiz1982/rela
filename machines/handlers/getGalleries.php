<?php 

	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';


function getImages(){
    global $wpdb;

	$mylink = $wpdb->get_results( $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND (post_mime_type = 'image/gif' OR post_mime_type = 'image/jpeg' OR post_mime_type = 'image/png')", ARRAY_A) );

	$returnArray = array();
	foreach ($mylink as $key => $value) {
		
		$returnArray[$value->ID] = buildAttachmentData($value->ID);
	}

	return $returnArray;

}

echo json_encode(getImages(), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);		

?>