<?php

	function slugify($text){
		$text = strtolower($text);
		$text = preg_replace('/\+/', '', $text); // Replace spaces with 
		$text = preg_replace('/\s+/', '-', $text); // Replace spaces with -
		$text = preg_replace('/[^\w\-]+/', '', $text); // Remove all non-word chars
		$text = preg_replace('/\-\-+/', '-', $text); // Replace multiple - with single -
		$text = preg_replace('/^-+/', '', $text); // Trim - from start of text
		$text = preg_replace('/-+$/', '', $text); // Trim - from end of text
		return $text;
	}

	$formData = $_POST['formData'];
	parse_str($formData, $formArray);

	$message = '<html><body>';
	$csvReturn = "";
	$excludeKeys = array('recaptcha_challenge_field', 'recaptcha_response_field');
	foreach ($formArray as $key => $value) {
		if(is_array($value)){
			foreach ($value as $innerKey => $innerValue) {
				if(!in_array($key, $excludeKeys)) {
					$message .= "<strong>" . $key . "</strong> " . $innerValue . "<br />";
					$csvSafeValue = str_replace(",", "", $innerValue);
					$csvReturn .= $csvSafeValue . ",";
				}
			}
		} else {
				if(!in_array($key, $excludeKeys)) {
					$message .= "<strong>" . $key . "</strong> " . $value . "<br />";
					$csvSafeValue = str_replace(",", "", $value);
					$csvReturn .= $csvSafeValue . ",";
				}
		}
	}

	$csvReturn = "\n" . substr($csvReturn, 0, -1);
	$csvValue = $_POST['csvUpdate'];
	$fp = fopen($csvValue . '.csv', 'a');
	fwrite($fp, $csvReturn);
	fclose($fp);

	$ccString = "CC: wfaye@bermangrp.com" . "\r\n";
	$ccString .= "BCC: wally.faye@gmail.com" . "\r\n";

	$to = 'it@bermangrp.com';
	$subject = 'Form Submission';
	$headers = "From: no_reply@new_website.com \r\n";
	$headers .= "Reply-To: no_reply@new_website.com \r\n";
	$headers .= $ccString;
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message .= '</body></html>';

	if (mail($to, $subject, $message, $headers)) {
		echo("success");
	} else {
		echo("fail");
	}


?>