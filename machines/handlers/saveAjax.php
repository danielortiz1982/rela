<?php
	// ini_set('display_errors', 1);
	// error_reporting(E_ALL);

	$pageIDrequest = $_POST['pageID'];
	$postIDrequest = $_POST['postID'];
	$HTMLrequest = $_POST['html'];
	
	$rootURL = '/var/www/html/wp-content/themes/rela/';
	$fileURL = ($postIDrequest == "") ? $rootURL . '_ajax/' . $pageIDrequest . '.html' : $rootURL . '_ajax/' . $pageIDrequest . '-' . $postIDrequest . '.html';

	ob_start();
		include_once($rootURL . 'views/output_seo_head_dom.php');
	$fileData = ob_get_clean();

	$newHTML = str_replace("</head>", $fileData, $HTMLrequest);

	$fp = fopen($fileURL, 'w');
	$fileWritten = fwrite($fp, $newHTML);
	fclose($fp);

	if ( $fileWritten === false ) {
		echo "fail";
	} else {
		echo "success";
	}

?>