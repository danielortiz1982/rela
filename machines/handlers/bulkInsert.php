<?php
	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

$json = '[
	{
		"state": "Alabama",
		"link": "https://myalabamataxes.alabama.gov/"
	},
	{
		"state": "Arizona",
		"link": "https://www.aztaxes.gov/Home"
	},
	{
		"state": "Arkansas",
		"link": "https://atap.arkansas.gov/_/"
	},
	{
		"state": "California",
		"link": "https://www.ftb.ca.gov/online/refund/index.asp"
	},
	{
		"state": "Colorado",
		"link": "https://www.colorado.gov/revenueonline/_/"
	},
	{
		"state": "Connecticut",
		"link": "http://www.ct.gov/drs/cwp/view.asp?a=1462&q=266274&drsPNavCtr=|40959|#40963"
	},
	{
		"state": "Delaware",
		"link": "http://revenue.delaware.gov/"
	},
	{
		"state": "Georgia",
		"link": "https://gtc.dor.ga.gov/_/#2"
	},
	{
		"state": "Hawaii",
		"link": "https://tax.ehawaii.gov/hoihoi/refund.html"
	},
	{
		"state": "Idaho",
		"link": "https://idahotap.gentax.com/TAP/_/"
	},
	{
		"state": "Illinois",
		"link": "https://www.revenue.state.il.us/app/refund/index.html"
	},
	{
		"state": "Indiana",
		"link": "https://secure.in.gov/apps/dor/tax/refund/"
	},
	{
		"state": "Iowa",
		"link": "https://www.idr.iowa.gov/wheresmyrefund/"
	},
	{
		"state": "Kansas",
		"link": "https://www.kdor.org/refundstatus/default.asp"
	},
	{
		"state": "Kentucky",
		"link": "http://www.revenue.ky.gov/refund.htm"
	},
	{
		"state": "Louisiana",
		"link": "https://esweb.revenue.louisiana.gov/WheresMyRefund/"
	},
	{
		"state": "Maine",
		"link": "https://portal.maine.gov/refundstatus/"
	},
	{
		"state": "Maryland",
		"link": "https://interactive.marylandtaxes.com/INDIV/refundstatus/home.aspx"
	},
	{
		"state": "Massachusetts",
		"link": "https://wfb.dor.state.ma.us/webfile/wsi/"
	},
	{
		"state": "Michigan",
		"link": "https://treas-secure.treas.state.mi.us/eservice_enu/start.swe?SWECmd=Start&SWEHo=treas-secure.treas.state.mi.us"
	},
	{
		"state": "Minnesota",
		"link": "https://www.mndor.state.mn.us/tp/refund/_/"
	},
	{
		"state": "Mississippi",
		"link": "https://tap.dor.ms.gov/_/#2"
	},
	{
		"state": "Missouri",
		"link": "https://dors.mo.gov/tax/taxinq/welcome.jsp"
	},
	{
		"state": "Montana",
		"link": "https://revenue.mt.gov/"
	},
	{
		"state": "Nebraska",
		"link": "https://ndr-refundstatus.ne.gov/refundstatus/public/search.faces"
	},
	{
		"state": "New Jersey",
		"link": "http://www.state.nj.us/treasury/taxation/refinfo.shtml"
	},
	{
		"state": "New Mexico",
		"link": "https://tap.state.nm.us/tap/_/#1"
	},
	{
		"state": "New York",
		"link": "https://www8.tax.ny.gov/PRIS/prisStart"
	},
	{
		"state": "North Carolina",
		"link": "http://www.dor.state.nc.us/electronic/individual/status.html"
	},
	{
		"state": "North Dakota",
		"link": "https://apps.nd.gov/tax/tap/_/"
	},
	{
		"state": "Ohio",
		"link": "http://www.tax.ohio.gov/Individual.aspx"
	},
	{
		"state": "Oklahoma",
		"link": "http://www.ok.gov/tax/"
	},
	{
		"state": "Oregon",
		"link": "https://secure.dor.state.or.us/refund/index.cfm"
	},
	{
		"state": "Pennsylvania",
		"link": "https://www.doreservices.state.pa.us/pitservices/wheresmyrefund.aspx"
	},
	{
		"state": "Rhode Island",
		"link": "https://www.ri.gov/taxation/refund/"
	},
	{
		"state": "South Carolina",
		"link": "https://www3.sctax.org/refundstatus/refund.aspx"
	},
	{
		"state": "Utah",
		"link": "https://tap.tax.utah.gov/TaxExpress/_/#2"
	},
	{
		"state": "Vermont",
		"link": "https://secure.vermont.gov/TAX/refund/"
	},
	{
		"state": "Virginia",
		"link": "http://www.tax.virginia.gov/content/where-my-refund"
	},
	{
		"state": "West Virginia",
		"link": "https://mytaxes.wvtax.gov/_/#1"
	},
	{
		"state": "Wisconsin",
		"link": "https://ww2.revenue.wi.gov/RefundInquiry/request.html"
	},
	{
		"state": "Washington D.C.",
		"link": "https://www.taxpayerservicecenter.com/individual/Ind_RefundStatus_Logon.jsp"
	}
]';

$obj = json_decode($json);

foreach ($obj as $key => $value) {
	// DO NOT UNCOMMENT UNLESS DOING A BULK ADD
		// $my_post = array(
		// 	"post_title"    => $value->state,
		// 	"post_status"   => "publish",
		// 	"post_type" => "states",
  //       	"post_content" => $value1->post_content
		// );

		// $newReplyPostID = wp_insert_post( $my_post );
		// add_post_meta( $newReplyPostID, "custom_order", 0 );
		// add_post_meta( $newReplyPostID, "state_refund_url", $value->link );

	print_r($value->state);
	echo "<br />";
	print_r($value->link);
	echo "<br />";
	echo "<br />";


}


?>