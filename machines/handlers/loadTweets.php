<?php

/*-----------------------------------------------------------------------------------*/
/*	Include Tweets
/*-----------------------------------------------------------------------------------*/
	include '../../machines/libraries/twitteroauth/tweets.php';

/*-----------------------------------------------------------------------------------*/
/*	return tweets
/*-----------------------------------------------------------------------------------*/
	if(isset($_POST['tweetsRequested'])){
		$tweetsRequested = $_POST['tweetsRequested'];
	} else {
		$tweetsRequested = 2;
	}
	echo json_encode(echoTweets($tweetsRequested));

?>