<?php

	$tableData = json_decode($_POST['tableData']);

	$fp = fopen('../../_data/' . $_POST['sheetName'] . '.json', 'w');


	if (fwrite($fp, json_encode($tableData)) === FALSE) {
		echo "error";
		exit;
	} else {
		fclose($fp);
		echo "success";
	}

?>