<?php 

	$fileURL = 'gitWebHook.json';
	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		file_put_contents($fileURL, file_get_contents("php://input"));
	} else {
		$writeData = "gitWebHook-test" . time();
		$fp = fopen('gitWebHook-test', 'w');
		fwrite($fp, $writeData);
		fclose($fp);
	}

	$jsonFile = file_get_contents($fileURL);
	$jsonData = json_decode($jsonFile);

	if($jsonData->ref == "refs/heads/master"){
		$writeData = "gitWebHook-master" . time();
		$fp = fopen('gitWebHook-master', 'w');
		fwrite($fp, $writeData);
		fclose($fp);

		shell_exec('/usr/bin/git -C /var/www/html/wp-content/themes/rela pull git@gitlab.com:bermangrp/rela.git');
	} else {
	}

/*
For this script to run, as a non-www-data user do:
	- generate key for www-data for gitlab
		- sudo -u www-data ssh-keygen -t rsa -C "wfaye@bermangrp.com"
	- add to SSH keys in gitlab UI
	- ensure www-data has privilages to write to directory
		- sudo chown -R www-data:www-data /var/www/wp-content/themes/jbb
		- sudo chmod g+w /var/www/wp-content/themes/jbb/.git/FETCH_HEAD 
	- test www-data's permissions
		- sudo -u www-data /usr/bin/git -C /var/www/wp-content/themes/jbb pull git@gitlab.com:wfaye/jbb.git


sudo chmod g+w /var/www/wp-content/themes/boilerplate/.git/FETCH_HEAD 
sudo chown -R www-data:www-data /var/www/wp-content/themes/boilerplate-dev

 */

?>