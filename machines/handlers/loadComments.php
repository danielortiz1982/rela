<?php
	$commentIDrequest = intval($_POST['id']);
	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

	$args = array(
		'status' => 'approve',
		'post_id' => $commentIDrequest
	);
	$commentData = get_comments( $args );

	echo json_encode($commentData);

?>