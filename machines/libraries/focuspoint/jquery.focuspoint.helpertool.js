var defaultImage;
var $dataAttrInput;
var $cssAttrInput;
var $focusPointContainers;
var $focusPointImages;
var $helperToolImage;
var focusPointAttr = {
	x: 0,
	y: 0,
	w: 0,
	h: 0
};
$ = jQuery.noConflict();
$(document).ready(function($) {
	
	(function() {

		defaultImage = 'img/city_from_unsplash.jpg';
		$dataAttrInput = $('.helper-tool-data-attr');
		$cssAttrInput = $('.helper-tool-css3-val');
		$helperToolImage = $('img.helper-tool-img, img.target-overlay');

		$focusPointContainers = $('.focuspoint');
		$focusPointImages = $('.focuspoint img');

		setImage( defaultImage );
	})();

	function setImage(imgURL) {
		$("<img/>")
		.attr("src", imgURL)
		.load(function() {
			focusPointAttr.w = this.width;
			focusPointAttr.h = this.height;
			
			$helperToolImage.attr('src', imgURL);
			
			$focusPointImages.attr('src', imgURL);

			$focusPointContainers.data('focusX', focusPointAttr.x);
			$focusPointContainers.data('focusY', focusPointAttr.y);
			$focusPointContainers.data('imageW', focusPointAttr.w);
			$focusPointContainers.data('imageH', focusPointAttr.h);
			
			$('.focuspoint').focusPoint();

			printDataAttr();
		});
	}

	function printDataAttr(){
		$dataAttrInput.val('data-focus-x="'+focusPointAttr.x.toFixed(2)+'" data-focus-y="'+focusPointAttr.y.toFixed(2)+'" data-focus-w="'+focusPointAttr.w+'" data-focus-h="'+focusPointAttr.h+'"');
	}

	$helperToolImage.on('click', function(e){
	
		var imageW = $(this).width();
		var imageH = $(this).height();
		
		var offsetX = e.pageX - $(this).offset().left;
		var offsetY = e.pageY - $(this).offset().top;
		var focusX = (offsetX/imageW - 0.5)*2;
		var focusY = (offsetY/imageH - 0.5)*-2;
		
		focusPointAttr.x = focusX;
		focusPointAttr.y = focusY;

		printDataAttr();

		updateFocusPoint();

		var percentageX = (offsetX/imageW)*100;
		var percentageY = (offsetY/imageH)*100;
		var backgroundPosition = percentageX.toFixed(0) + '% ' + percentageY.toFixed(0) + '%';
		var backgroundPositionCSS = 'background-position: ' + backgroundPosition + ';';
		$cssAttrInput.val(backgroundPositionCSS);

		$('.reticle').css({
			'top':percentageY+'%',
			'left':percentageX+'%'
		});
	});
	
	function updateFocusPoint(){

		$focusPointContainers.data('focusX', focusPointAttr.x);
		$focusPointContainers.data('focusY', focusPointAttr.y);
		$focusPointContainers.adjustFocus();
	}
});