<?php

/* Add user fields
================================================== */
	$userFieldsArray = array(
		"user_verified_meta" => array(
			"name" => "Verified",
			"callback_args" => array(
				"input_type" => "checkbox_single",
				"input_name" => "user_verified_meta"
				)
			),
		);

	add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
	add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
	function my_show_extra_profile_fields( $user ) {
		global $userFieldsArray;
		// echo '<h3>Extra profile information</h3>';
		echo '<table class="form-table">';
		foreach ($userFieldsArray as $key => $value) {
			switch ($value['callback_args']['input_type']) {
				case 'input_text':
					$inputValue = esc_attr( get_the_author_meta( $value['callback_args']['input_name'], $user->ID ) );
					$inputID = $value['callback_args']['input_name'];
					$labelText = $value['name'];
					include(TEMPDIR . '/views/input_checkbox_single.php');
					//include PAGEDIR . '/views/user_input_text.php';
				break;

				case 'checkbox_single':
					$inputValue = esc_attr( get_the_author_meta( $value['callback_args']['input_name'], $user->ID ) );
					$inputID = $value['callback_args']['input_name'];
					$labelText = $value['name'];
					include(TEMPDIR . '/views/input_user_checkbox_single.php');
				break;
			}
		}
		echo '</table>';
	}

	add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
	add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

	function my_save_extra_profile_fields( $user_id ) {
		global $userFieldsArray;
		if ( !current_user_can( 'edit_user', $user_id ) ){
			return false;
		}

		foreach ($userFieldsArray as $key => $value) {
			$userValue = ($_POST[$value['callback_args']['input_name']] == "Y") ? "Y" : "N";
			update_user_meta( $user_id, $value['callback_args']['input_name'], $userValue );
		}
	}

	function userVerified($userID){
		$valueData = esc_attr( get_the_author_meta( 'user_verified_meta', $userID ) );
		return ($valueData == "Y") ? true : false;
	}

	//add additional columns to the users.php admin page
	function modify_user_columns($columnSettings) {
	    $columnSettings['user_verified_meta_column'] = __('Verified', 'user-column');
	    return $columnSettings;
	}
	add_filter('manage_users_columns','modify_user_columns');

	//add content to your new custom column
	function modify_user_column_content($val,$column_name,$user_id) {
	    $user = get_userdata($user_id);
		switch ($column_name) {
	        case 'user_verified_meta_column':
	        	return get_the_author_meta( 'user_verified_meta', $user_id );
	            break;
	        default:
	    }
	    return $return;
	}
	add_filter('manage_users_custom_column','modify_user_column_content',10,3);

	//make the new column sortable
	function user_sortable_columns( $columns ) {
		$columns['user_verified_meta_column'] = 'user_verified_meta_column';
		return $columns;
	}
	add_filter( 'manage_users_sortable_columns', 'user_sortable_columns' );

	//set instructions on how to sort the new column
	if(is_admin()){
		add_action('pre_user_query', 'my_user_query');
	}

	function my_user_query($vars){	
		global $wpdb;

		if ( isset( $vars->query_vars['orderby'] ) && 'user_verified_meta_column' == $vars->query_vars['orderby'] ) {

			$vars->query_from .= " LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) ";
			$vars->query_where .= " AND alias.meta_key = 'user_verified_meta'";
			$vars->query_orderby = " ORDER BY alias.meta_value ".($vars->query_vars["order"] == "ASC" ? "asc " : "desc ");

		}

		return $vars;
	}

	function newUserSetup($userIDrequest){
    	update_user_meta( $userIDrequest, "user_verified_meta", "N" );
	}

	function listUsers(){
		// $args = array('exclude' => array(1, 1164, 4, 1165, 1157, 1095, 1160));
		// $allUsers = get_users($args);
		$allUsers = get_users();
		$AlluserMetaArr = array();
		foreach ($allUsers as $key => $value) {

			$thisUserData = getUserData($value->ID);
			$newUserData = array();
			$okArray = array(
				'ID',
				'user_email',
				'first_name',
				'last_name',
				'user_work_title_meta',
				'user_firm_organization_meta',
				'user_address_meta',
				'user_city_town_meta',
				'user_state_meta',
				'user_zip_code_meta',
				'user_direct_phone_meta',
				'user_verified_meta'
			);
			foreach ($thisUserData as $key1 => $value1) {
				switch ($key1) {
					case 'user_email':
						$newValue1 = htmlspecialchars($value1, ENT_QUOTES);
						$newValue1 = preg_replace("/\s+/", " ", $newValue1);
						$newUserData['name'] = $newValue1;
					break;
					
					case 'ID':
						$newValue1 = htmlspecialchars($value1, ENT_QUOTES);
						$newValue1 = preg_replace("/\s+/", " ", $newValue1);
						$newUserData['id'] = $newValue1;
					break;
					
					default:
						if(in_array($key1, $okArray)){
							$newValue1 = htmlspecialchars($value1, ENT_QUOTES);
							$newValue1 = preg_replace("/\s+/", " ", $newValue1);
							$newUserData[$key1] = $newValue1;
						}
					break;
				}
			}

			$AlluserMetaArr[] = $newUserData;
			// $AlluserMetaArr[] = $thisUserData;
		}
		return $AlluserMetaArr;
	}

	function getUserData($userID){
        global $wpdb;
		$usersTableData = $wpdb->get_results( $wpdb->prepare("SELECT * FROM $wpdb->users WHERE ID = '$userID'", ARRAY_A) );
		$mylink = $wpdb->get_results( $wpdb->prepare("SELECT meta_key,meta_value FROM $wpdb->usermeta WHERE user_id = '$userID'", ARRAY_A) );
		foreach ($mylink as $value) {
			$fieldName = $value->meta_key;
			$fieldValue = $value->meta_value;
			$usersTableData[0]->$fieldName = $fieldValue;
		}
		return $usersTableData[0];
	}


?>