<?php

// DEFINE META BOXES
	$pageMetaBoxArray = array(
	    "page_featured_images_meta" => array(
	    	"id" => "page_featured_images_meta",
	        "name" => "Featured Images",
	        "post_type" => "page",
	        "position" => "side",
	        "priority" => "low",
	        "callback_args" => array(
	        	"input_type" => "input_featured_images",
	        	"input_name" => "page_featured_images"
	        )
	    ),
	    "page_secondary_editor_meta" => array(
	    	"id" => "page_secondary_editor_meta",
	        "name" => "Secondary Editor",
	        "post_type" => "page",
	        "position" => "side",
	        "priority" => "low",
	        "callback_args" => array(
	        	"input_type" => "input_editor",
	        	"input_name" => "secondary_editor"
	        )
	    ),
	);

// ADD META BOXES
	add_action( "admin_init", "admin_init_page" );
	function admin_init_page(){
		global $pageMetaBoxArray;
		generateMetaBoxes($pageMetaBoxArray);
	}

// SAVE POST TO DATABASE
	add_action('save_post', 'save_page');
	function save_page(){
		global $pageMetaBoxArray;
		savePostData($pageMetaBoxArray, $post, $wpdb);
	}

// LISTING FUNCTION
	function listPages($context, $idArray = null){
		global $post;
		global $pageMetaBoxArray;
		
		switch ($context) {
			case 'sort':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);
				$loop = new WP_Query($args);

				echo '<ul class="sortable">';
				while ($loop->have_posts()) : $loop->the_post(); 
					$output = get_the_title($post->ID);//get_post_meta($post->ID, 'first_name', true) . " " . get_post_meta($post->ID, 'last_name', true);
					include(TEMPDIR . '/views/item_sortable.php');
				endwhile;
				echo '</ul>';
			break;
			
			case 'json':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);
				returnData($args, $pageMetaBoxArray, 'json', 'page_data');
			break;

			case 'array':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);
				return returnData($args, $pageMetaBoxArray, 'array');
			break;

			case 'rest':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true,
					'post__in' => $idArray
				);
				return returnData($args, $pageMetaBoxArray, 'array');
			break;


			case 'inputs':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);

				$outputArray = returnData($args, $peopleMetaBoxArray, 'array');

				$field_options = array();
				foreach ($outputArray as $key => $value) {
					$checkBoxOption = array(
						"id" => $value['post_id'],
						"name" => html_entity_decode($value['the_title']),
					);
					$field_options[] = $checkBoxOption;
				}

				return $field_options;

			break;

		}
	}

?>